<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The gradebook new report
 *
 * @package   gradereport_new
 * @copyright 2007 Nicolas Connault
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later

 */

// File: /mod/mymodulename/view.php
require_once '../../../config.php';//has CFG variable
require_once $CFG->dirroot.'/grade/report/new/lib.php';
require_once($CFG->libdir.'/tablelib.php');

//COURSE ID AS REQUIRED PARAM
$courseid = required_param('id', PARAM_INT);
// ACTIVITY ID OR STUDENT ID AS OPTIONAL, SO WE KNOW THE TABLE TO BUILD ACCORDING TO THE PARAMETER
//$groupid  = optional_param('group', null, PARAM_INT);
$userid = optional_param('user', null, PARAM_INT); // User ID.
$activity = optional_param('act', null, PARAM_INT); // Activity ID.

//data for context
$course = $DB->get_record('course', array('id' => $courseid));
//require login - VALIDATION
require_login($course);

// Set up the page.
$PAGE->set_url(new moodle_url('/grade/report/new/index.php', array('id'=>$courseid)));
// Context
$contextCourse = context_course::instance($courseid);
// Layout
$PAGE->set_pagelayout('report');

//START SHOWING CONTENT ON THE PAGE
echo $OUTPUT->header();

$table = new flexible_table('new-report-table');//[uniqueid]

echo $OUTPUT->heading(get_string('pluginname', 'gradereport_new'));//format_string('someName')

// ************ BUILD DROPDOWN MENUS ************ //

//get data
$classes = gradereport_new::getCourseClass($courseid);
//build select
$selectClass = gradereport_new::buildSelect($classes,'classid','Select Class');
//show select on the view
echo "$selectClass";

//get data
$sections = gradereport_new::getCourseSection($courseid);
//build select
$selectSection = gradereport_new::buildSelect($sections,'sectionid','Select Unit');
//show select on the view
echo "$selectSection";

//get data
$activities = gradereport_new::getActivityClass($courseid);
//build select
$selectActv = gradereport_new::buildSelect($activities,'actvid','Select Activity');
//show select on the view
echo "$selectActv";


// ************ BUILD REPORT TABLE ************ //

$scormData = gradereport_new::getScormData($courseid,$contextCourse->id);

die;


echo $OUTPUT->box_start('generalbox boxaligncenter');
echo $OUTPUT->heading("All Attempts", 3);

$table->define_baseurl($PAGE->url);

//SET TABLE CLASSES
//$table->set_attribute('class', 'generaltable generalbox boxaligncenter');
//SET TABLE ID
$table->set_attribute('id', 'new-report-table1');

//table columns [columns]
$table->define_columns(array('id','pupil', 'score', 'attmpt', 'time','q1','q2','q3'));

//table headers [headers]
$table->define_headers(
        array(
                format_string('Pupil Id'),
                format_string('Pupil Name'),
                format_string('Final Score'),
                format_string('Attempts'),
                format_string('Time'),
                format_string('Q1'),
                format_string('Q2'), 
                format_string('Q3')
            )
        );

$table->setup();

//GET THIS COURSE STUDENTS
$query =   'SELECT u.id AS id, firstname, lastname 
			FROM mdl_role_assignments AS a, mdl_user AS u 
			WHERE contextid = '.$contextCourse->id.' AND roleid = 5 AND a.userid = u.id;';
$students = $DB->get_recordset_sql( $query ); 

foreach( $students as $studentId => $studentData) {
	$dataByRow = array(); 
	$dataByRow[] = $studentId;
	$dataByRow[] = $studentData->firstname;
	//$table->column_style_all('display', 'block');
	if($studentData->firstname=='Maria'){
		//$table->column_class[''] = 'result1';
		//$table->column_style('display', 'block');
		$dataByRow[] = '80';//    display: block;
		$dataByRow[] = '1';
		$dataByRow[] = '100s';
		$dataByRow[] = '10';
		$dataByRow[] = '0';
		$dataByRow[] = '5';	
	}else{
		$dataByRow[] = '50';
		$dataByRow[] = '1';
		$dataByRow[] = '50s';
		$dataByRow[] = '0';
		$dataByRow[] = '0';
		$dataByRow[] = '100';
	}
	
	$table->add_data($dataByRow);
}
//print_object($table);die;
$table->finish_output();

echo $OUTPUT->box_end();
echo $OUTPUT->footer();