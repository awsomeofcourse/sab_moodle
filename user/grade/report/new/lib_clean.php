<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Base lib class for new functionality.
 *
 * @package   gradereport_new
 * @copyright 2014 Moodle Pty Ltd (http://moodle.com)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/grade/report/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once("../../../config.php");

/**
 * This class is the main class that must be implemented by a grade report plugin.
 *
 * @package   gradereport_new
 * @copyright 2014 Moodle Pty Ltd (http://moodle.com)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class gradereport_new extends grade_report {

    /**
     * Return the list of valid screens, used to validate the input.
     *
     * @return array List of screens.
     */
    public static function valid_screens() {
        // This is a list of all the known classes representing a screen in this plugin.
        return array('user', 'select', 'grade');
    }

    /**
     * Process data from a form submission. Delegated to the current screen.
     *
     * @param array $data The data from the form
     * @return array List of warnings
     */
    public function process_data($data) {
        if (has_capability('moodle/grade:edit', $this->context)) {
            return $this->screen->process($data);
        }
    }

    /**
     * Unused - abstract function declared in the parent class.
     *
     * @param string $target
     * @param string $action
     */
    public function process_action($target, $action) {
    }

    /**
     * Constructor for this report. Creates the appropriate screen class based on itemtype.
     *
     * @param int $courseid The course id.
     * @param object $gpr grade plugin return tracking object
     * @param context_course $context
     * @param string $itemtype Should be user, select or grade
     * @param int $itemid The id of the user or grade item
     * @param string $unused Used to be group id but that was removed and this is now unused.
     */
    public function __construct($courseid, $gpr, $context, $itemtype, $itemid, $unused = null) {
        parent::__construct($courseid, $gpr, $context);

        $base = '/grade/report/new/index.php';

        $idparams = array('id' => $courseid);

        $this->baseurl = new moodle_url($base, $idparams);

        $this->pbarurl = new moodle_url($base, $idparams + array(
                'item' => $itemtype,
                'itemid' => $itemid
            ));

        //  The setup_group method is used to validate group mode and permissions and define the currentgroup value.
        $this->setup_groups();

        $screenclass = "\\gradereport_new\\local\\screen\\${itemtype}";

        $this->screen = new $screenclass($courseid, $itemid, $this->currentgroup);

        // Load custom or predifined js.
        $this->screen->js();
    }

    /**
     * Build the html for the screen.
     * @return string HTML to display
     */
    public function output() {
        global $OUTPUT;
        return $OUTPUT->box($this->screen->html());
    }


    //------------------------------------------------------------FUNCTIONS
    public static function FUNCTIONNAME($userid,$scoid) {       
        global  $DB;
        //echo "for STUDENT $userid and $scoid";

        $attempt = optional_param('attempt', 1, PARAM_INT); // attempt 

           
        $tracks = $DB->get_records('scorm_scoes_track', 
                                    array('userid' => $userid, 
                                            'scoid' => $scoid,
                                            'attempt' => $attempt)
                                            , '','*');

        //BEGIN TO PROCESS DATA RECEIVED
        $curr_id = 0;
        $group = array();
        $other = array();
        
        $has_= false;
        // Distinguish parameters from Puzzle or Technology Activity
        foreach ($tracks as $key => $obj) {
            $has_= false;
            if (stristr($obj->element, 'cmi.interactions_') == true) {
                $has_= true;break;
            }
        }

        // Here I'm going to group the records for each question
        if ($has_) {
            // Puzzle Activity - echo "Puzzle Activity";
            foreach ($tracks as $record => $obj) {
                //Swith between Interactions, Objectives or other recods
                $curElement = stristr(str_replace('cmi.', '', $obj->element), '_',true);
                switch ($curElement) {
                    
                    case 'interactions':
                        
                        //get the ID and Name of the current element
                        $elemntIdName = str_replace(strstr($obj->element, '_',true).'_', '', $obj->element);
                        //get only the ID
                        $interaction_id = strstr($elemntIdName, '.',true);
                        //for each different question -> create a new group
                        $group[$interaction_id]['question_id'] = $interaction_id;
                        //get the name, to add to the group
                        $interaction_name = str_replace('.', '', strstr($elemntIdName, '.'));
                        //Compare to the current one to check if we can add to the same group
                        if ($curr_id==$interaction_id) {
                            //add to the current group
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }else{
                            //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                            if($interaction_name=='id'){
                                $group[$interaction_id][$interaction_name] = $obj->value;
                            }
                        }

                        //Update current id to be compared
                        $curr_id = $interaction_id;
                        
                        break;

                    case 'objectives':

                        $objectives = str_replace('cmi.objectives_', '', $obj->element);
                        //get only the ID
                        $interaction_id = str_replace('cmi.objectives_', '', $obj->element);
                        $interaction_id = strstr(str_replace('cmi.objectives_', '', $obj->element), '.',true);

                        //get the name, to add to the group
                        $interaction_name = str_replace('cmi.objectives_'.$interaction_id.'.', '', $obj->element);

                        //Compare to the current one to check if we can add to the same group
                        if ($curr_id==$interaction_id) {
                            //add to the current group
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }else{
                            //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                            if($interaction_name=='score.raw'){
                                $group[$interaction_id][$interaction_name] = $obj->value;
                            }
                        }
                        //Update current id to be compared
                        $curr_id = $interaction_id;

                        break;
                    
                    default:
                        
                        if (stristr($obj->element, 'core') == true) {
                            $otherName = str_replace('cmi.core.', '', $obj->element);
                            $other[$otherName] = $obj->value;
                        }else{
                            $date = date('d/m/Y H:i:s', $obj->value);
                            $other[$obj->element] = $obj->value.' -> '.$date;
                        }
                        break;
                }
            }
        }
        else{
            //echo "Technology Activity";
            foreach ($tracks as $record => $obj) {

                //Swith between Interactions, Objectives or other recods
                $piece =str_replace('cmi.', '', $obj->element);
                $curElement = stristr($piece, '.',true);
                
                if (stristr($obj->element, 'cmi.interactions') !== false) {
                    //get the ID and Name of the current element
                    $elemntIdName = str_replace('cmi.interactions.', '',$obj->element);
                    //get only the ID
                    $interaction_id = strstr($elemntIdName, '.',true);
                    //for each different question -> create a new group
                    $group[$interaction_id]['question_id'] = $interaction_id;

                    //Ignore the elements that don't match to what we are looking for
                    if(stristr($obj->element, '.objectives.') !== false || stristr($obj->element, '.correct_responses.') !== false){

                        //get the first name
                        $interaction_1name = strstr(str_replace(strstr($elemntIdName, '.',true).'.', '',  $elemntIdName), '.',true);
                        //get the second name
                        $interaction_2name = str_replace('0.', '', strstr($obj->element, '0.'));
                        //its an exception for the case when the the id changes
                        if(stristr($interaction_2name, '.') !== false){
                            $interaction_2name = str_replace($interaction_1name.'.', '', $interaction_2name);
                        }
                        //add the two names, on a single string
                        $interaction_name = $interaction_1name.'_'.$interaction_2name;
                    }
                    else{
                        //get the name, to add to the group
                        $interaction_name = str_replace('.', '', strstr($elemntIdName, '.'));
                    }
                    
                    //Compare to the current one to check if we can add to the same group
                    if ($curr_id==$interaction_id) {
                        //add to the current group
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }else{
                        //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                        if($interaction_name=='objectives_id'){
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }
                    }

                    //Update current id to be compared
                    $curr_id = $interaction_id;
                }
                else{
                    if (stristr($obj->element, 'cmi.') == true) {
                        $field = str_replace('cmi.','', $obj->element);
                        $other[$field] = $obj->value;
                        if(stristr($obj->element, '_time') == true){
                            $other[$field] = scorm_format_duration($obj->value);
                        }
                    }else{
                        $date = date('d/m/Y H:i:s', $obj->value);
                        $other[$obj->element] = $obj->value.' -> '.$date;
                    }
                }
            }
        }

        //Note: don't forget to validate the context and check capabilities
     
        return $group;
    }

    public static function getCourseClass($courseid) {       
        global  $DB;
        
        //get classes
        $cquery =   'SELECT id, name  
                    FROM mdl_groups as c
                    WHERE courseid = '.$courseid.';';
        $classesData = $DB->get_recordset_sql( $cquery ); 

        $classes = array();
        foreach ($classesData as $key => $class) {
            $classes[] = array('id'=>$class->id,'name'=>$class->name);
        }

      return $classes;
    }

    public static function getCourseSection($courseid) {       
        global  $DB;
        
        //the first record doesnt have name because its the same for all courses - a first section where the news forum is, so should we include that?
        $sectionsData = $DB->get_records('course_sections', array('course' => $courseid) , '', 'id, name');

        $sections = array();
        foreach ($sectionsData as $key => $sect) {
            $sections[] = array('id'=>$sect->id,'name'=>$sect->name);
        }

      return $sections;
    }

    public static function getActivityClass($courseid) {       
        global  $DB;
        
        //get activ
        $cquery =   'SELECT 
                        cm.id, 
                        cm.module, 
                        m.name 
                    FROM 
                        mdl_course_modules cm 
                        LEFT JOIN mdl_modules m on m.id = cm.module 
                    WHERE 
                        cm.course = '.$courseid.';';
        $activData = $DB->get_recordset_sql( $cquery ); 

        $ScormAct = array();
        foreach ($activData as $key => $class) {
            //print_object($class);
            //$activ[] = array('id'=>$class->id,'name'=>$class->name);
            $activType = $class->name;
            //print_object($activType);
            switch ($activType) {
                case 'scorm':
                    $ScormAct[] = array('id'=>$class->id,'name'=>$class->name);
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        $scorms = array();
        if(!empty($ScormAct)){
            //get scorm names
            $scquery =   'SELECT 
                        id, name 
                    FROM 
                        mdl_scorm as c
                    WHERE 
                        course = '.$courseid.';';
            $scoms = $DB->get_recordset_sql( $scquery ); 
            //print_object($scoms);

            foreach ($scoms as $key => $class) {
                $scorms[] = array('id'=>$class->id,'name'=>$class->name);
            }
        }else{
            echo "NO";
        }
      return $scorms;
    }

    public static function buildSelect($data,$name,$selectWhat){
        $beginSelect = '<select id="" class="select autosubmit singleselect" name="'.$name.'">';
        $endSelect = '</select>';
        //build select content
        $select = '';
        if(!empty($data)){
            $selectOption='<option selected="selected" value="">'.$selectWhat.'</option>';
            $selectContent='';
            for ($i=0; $i <count($data) ; $i++) { 
                $selectContent.='<option value="'.$data[$i]['id'].'">'.$data[$i]['name'].'</option>';
            }
            $select = $beginSelect.$selectOption.$selectContent.$endSelect;
        }
        return $select;
    }

    /**
     * Returns the full list of attempts a user has made.
     *
     * @param int $scormid the id of the scorm.
     * @param int $userid the id of the user.
     *
     * @return array array of attemptids
     */
    public function scorm_get_all_attempts($scormid, $userid) {
        global $DB;
        $attemptids = array();
        $sql = "SELECT DISTINCT attempt FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? ORDER BY attempt";
        $attempts = $DB->get_records_sql($sql, array($userid, $scormid));
        foreach ($attempts as $attempt) {
            $attemptids[] = $attempt->attempt;
        }
        return $attemptids;
    }

    public static function getScormData($courseid,$contextid){
        global  $DB;

        //GET THIS COURSE STUDENTS
        $query =   'SELECT u.id AS id, firstname, lastname 
                    FROM mdl_role_assignments AS a, mdl_user AS u 
                    WHERE contextid = '.$contextid.' AND roleid = 5 AND a.userid = u.id;';
        $students = $DB->get_recordset_sql( $query ); 

        $ids = array();
        foreach( $students as $studentId => $studentData) {
            $ids[] = $studentId;
        }
        //print_object($ids);
        echo "<br><br>";
        //$scorms = $this->getActivityClass($courseid);
        
        //get scorm names
        $scquery =   'SELECT 
                    id, name 
                FROM 
                    mdl_scorm as c
                WHERE 
                    course = '.$courseid.';';
        $scorms = $DB->get_recordset_sql( $scquery ); 
        //print_object($scorms);

        foreach ($scorms as $key => $scorm) {
            //$scorms[] = array('id'=>$scorm->id,'name'=>$scorm->name);
            echo "scorm ".$scorm->name.'<br>';
            // Get list of attempts this user has made.
            for ($i=0; $i <count($ids) ; $i++) {
                echo "id ".$scorm->id.' user '.$ids[$i].'<br>'; 
                //$attemptids = $this->scorm_get_all_attempts($scorm->id, $ids[$i]);
                $sql = "SELECT DISTINCT attempt FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? ORDER BY attempt";
                $attempts = $DB->get_records_sql($sql, array($ids[$i], $scorm->id));

                print_object($attempts);
                
            }
            echo "<br><br>";
        }
        
        
    }

    public function get_value() {
        $this->label = $this->grade->grade_item->itemname;

        $val = $this->grade->finalgrade;
        if ($this->grade->grade_item->scaleid) {
            return $val ? (int)$val : -1;
        } else {
            return $val ? format_float($val, $this->grade->grade_item->get_decimals()) : '';
        }
    }

    //$this->get_value()


    public static function getScorms($courseid){
        global  $DB;
        //get scorm names
        $scquery =   'SELECT 
                    id, name, version, grademethod 
                FROM 
                    mdl_scorm as c
                WHERE 
                    course = '.$courseid.';';
                    //echo "$scquery";
        $scoms = $DB->get_recordset_sql( $scquery );
        //print_object($scoms);
        return $scoms;
    }

    /**
     * displays the full report
     * @param \stdClass $allowedlist list of students
     * @param \stdClass $scorm full SCORM object
     * @param \stdClass $cm - full course_module object
     * @param \stdClass $course - full course object
     * @param string $download - type of download being requested
     */
    public static function getScormData2($allowedlist,$scorm, $cm, $course, $download='') {
        global $DB, $OUTPUT;

        $contextmodule = \context_module::instance($cm->id);
        $attemptsmode = 0;

        // Find out current groups mode.
        //$currentgroup = groups_get_activity_group($cm, true);
        $currentgroup = array();
       
        // Select group menu.
        $displayoptions['objectivescore'] = array();
        
        $formattextoptions = array('context' => \context_course::instance($course->id));
        
        // Select the students.
        $nostudents = false;
        if (empty($currentgroup)) {
            // All users who can attempt scoes.
            if (!$students = get_users_by_capability($contextmodule, 'mod/scorm:savetrack', 'u.id', '', '', '', '', '', false)) {
                echo get_string('nostudentsyet');
                $nostudents = true;
            }
        } else {
            // All users who can attempt scoes and who are in the currently selected group.
            $groupstudents = get_users_by_capability($contextmodule, 'mod/scorm:savetrack',
                                                     'u.id', '', '', '', $currentgroup, '', false);
            if (!$groupstudents) {
                echo get_string('nostudentsingroup');
                $nostudents = true;
            }
        }
        unset($groupstudents);
        
        if (!$nostudents) {
            // Now check if asked download of data.
            $coursecontext = \context_course::instance($course->id);

            // Define table columns.
            $columns = array();
            $headers = array();
            $columns[] = 'fullname';
            $headers[] = get_string('name');
            $columns[] = 'attempt';
            $headers[] = get_string('attempt', 'scorm');
            $columns[] = 'start';
            $headers[] = get_string('started', 'scorm');
            $columns[] = 'score';
            $headers[] = get_string('score', 'scorm');
            //GET SCOES
            $scoes = $DB->get_records('scorm_scoes', array("scorm" => $scorm->id), 'sortorder, id');
            foreach ($scoes as $sco) {
                if ($sco->launch != '') {
                    $columns[] = 'scograde'.$sco->id;
                    $headers[] = format_string($sco->title, '', $formattextoptions);
                }
            }

            $params = array();
            list($usql, $params) = $DB->get_in_or_equal($allowedlist, SQL_PARAMS_NAMED);
            // Construct the SQL.
            $select = 'SELECT DISTINCT '.$DB->sql_concat('u.id', '\'#\'', 'COALESCE(st.attempt, 0)').' AS uniqueid, ';
            $select .= 'st.scormid AS scormid, st.attempt AS attempt, ' .
                    \user_picture::fields('u', array('idnumber'), 'userid') .
                    get_extra_user_fields_sql($coursecontext, 'u', '', array('email', 'idnumber')) . ' ';

            // This part is the same for all cases - join users and scorm_scoes_track tables.
            $from = 'FROM {user} u ';
            $from .= 'LEFT JOIN {scorm_scoes_track} st ON st.userid = u.id AND st.scormid = '.$scorm->id;
            // Show all students with or without attempts.
            $where = ' WHERE u.id ' .$usql. ' AND (st.userid IS NOT NULL OR st.userid IS NULL)';

            $countsql = 'SELECT COUNT(DISTINCT('.$DB->sql_concat('u.id', '\'#\'', 'COALESCE(st.attempt, 0)').')) AS nbresults, ';
            $countsql .= 'COUNT(DISTINCT('.$DB->sql_concat('u.id', '\'#\'', 'st.attempt').')) AS nbattempts, ';
            $countsql .= 'COUNT(DISTINCT(u.id)) AS nbusers ';
            $countsql .= $from.$where;
            $nbmaincolumns = count($columns); // Get number of main columns used.

            $objectives = gradereport_new::get_scorm_objectives2($scorm->id);
           
            $nosort = array();
            foreach ($objectives as $scoid => $sco) {
                foreach ($sco as $id => $objectivename) {
                    $colid = $scoid.'objectivestatus' . $id;
                    $columns[] = $colid;
                    $nosort[] = $colid;

                    if (!$displayoptions['objectivescore']) {
                        // Display the objective name only.
                        $headers[] = $objectivename;
                    } else {
                        // Display the objective status header with a "status" suffix to avoid confusion.
                        $headers[] = $objectivename. ' '. get_string('status', 'scormreport_objectives');

                        // Now print objective score headers.
                        $colid = $scoid.'objectivescore' . $id;
                        $columns[] = $colid;
                        $nosort[] = $colid;
                        $headers[] = $objectivename. ' '. get_string('score', 'scormreport_objectives');
                    }
                }
            }

        } else {
            echo get_string('noactivity', 'scorm');
        }

        $data = array(
            'columns' => $columns,
            'headers' => $headers,
            'nosort'  => $nosort,
            'scoes'   => $scoes,
            'params'  => $params,
            'countsql'=> $countsql,
            'select'  => $select,
            'from'    => $from,
            'where'   => $where,
            'nbmaincolumns' => $nbmaincolumns,
            'objectives' => $objectives
        );
        return $data;
    }

    /**
     * Returns The maximum numbers of Objectives associated with an Scorm Pack
     *
     * @param int $scormid Scorm instance id
     * @return array an array of possible objectives.
     */
    static function get_scorm_objectives2($scormid) {
        global $DB;
        
        $objectives = array();
        $params = array();
        $select = "scormid = ? AND ";
        $select .= $DB->sql_like("element", "?", false);
        $params[] = $scormid;
        $params[] = "cmi.objectives_%.id";
        $rs = $DB->get_recordset_select("scorm_scoes_track", $select, $params, 'value', 'DISTINCT value, scoid');
        if ($rs->valid()) {
            foreach ($rs as $record) {
                $objectives[$record->scoid][] = $record->value;
            }
            // Now naturally sort the sco arrays.
            foreach ($objectives as $scoid => $sco) {
                natsort($objectives[$scoid]);
            }
        }
        $rs->close();
        return $objectives;
    }

    public static function getStudents($contextCourseId){
        global $DB;

        $query = 'SELECT u.id AS id, firstname, lastname 
                FROM mdl_role_assignments AS a, mdl_user AS u 
                WHERE contextid = '.$contextCourseId.' AND roleid = 5 AND a.userid = u.id;';
        $students = $DB->get_recordset_sql($query);

        $allowedlist = array();
        foreach( $students as $studentId => $studentData) {
            $allowedlist[] = $studentId;
        }
        return $allowedlist;
    }
}
