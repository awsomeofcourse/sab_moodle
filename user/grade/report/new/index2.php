<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The gradebook new report
 *
 * @package   gradereport_new
 * @copyright 2007 Nicolas Connault
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later

 */

// File: /mod/mymodulename/view.php
require_once '../../../config.php';//has CFG variable
require_once $CFG->dirroot.'/grade/report/new/lib.php';
require_once($CFG->libdir.'/tablelib.php');

//COURSE ID AS REQUIRED PARAM
$courseid = required_param('id', PARAM_INT);
// ACTIVITY ID OR STUDENT ID AS OPTIONAL, SO WE KNOW THE TABLE TO BUILD ACCORDING TO THE PARAMETER
//$groupid  = optional_param('group', null, PARAM_INT);
$userid = optional_param('user', null, PARAM_INT); // User ID.
$activity = optional_param('act', null, PARAM_INT); // Activity ID.

//data for context
$course = $DB->get_record('course', array('id' => $courseid));
//require login - VALIDATION
require_login($course);

// Set up the page.
$PAGE->set_url(new moodle_url('/grade/report/new/index.php', array('id'=>$courseid)));
// Context
$contextCourse = context_course::instance($courseid);
// Layout
$PAGE->set_pagelayout('report');

//START SHOWING CONTENT ON THE PAGE
echo $OUTPUT->header();

//$table = new flexible_table('new-report-table');//[uniqueid]

echo $OUTPUT->heading(get_string('pluginname', 'gradereport_new'));//format_string('someName')

// ************ BUILD DROPDOWN MENUS ************ //

//get data
$classes = gradereport_new::getCourseClass($courseid);
//build select
$selectClass = gradereport_new::buildSelect($classes,'classid','Select Class');
//show select on the view
echo "$selectClass";

//get data
$sections = gradereport_new::getCourseSection($courseid);
//build select
$selectSection = gradereport_new::buildSelect($sections,'sectionid','Select Unit');
//show select on the view
echo "$selectSection";

//get data
$activities = gradereport_new::getActivityClass($courseid);
//build select
$selectActv = gradereport_new::buildSelect($activities,'actvid','Select Activity');
//show select on the view
echo "$selectActv";


// ************ BUILD REPORT TABLE ************ //

//GET THIS COURSE STUDENTS
$query =   'SELECT u.id AS id, firstname, lastname 
            FROM mdl_role_assignments AS a, mdl_user AS u 
            WHERE contextid = '.$contextCourse->id.' AND roleid = 5 AND a.userid = u.id;';
$students = $DB->get_recordset_sql( $query ); 

$allowedlist = array();
foreach( $students as $studentId => $studentData) {
    $allowedlist[] = $studentId;
}

//$scormData = gradereport_new::getScormData($courseid,$contextCourse->id);

	$scoms = gradereport_new::getScorms($courseid);
	$download='';

	//for ($i=0; $i <count($activities) ; $i++) {
	$displayoptions['attemptsmode'] = 0;
    $displayoptions['objectivescore'] = 1;
	foreach ($scoms as $key => $scorm) {

		//$activitId=$activities[$i]['id'];
		//echo "start";
		//print_object($scorm);
		$data = gradereport_new::getScormData2($allowedlist,$scorm,$contextCourse,$course);	
		//print_object($data);
		// Used when an empty cell is being printed - in html we add a space.
	    $emptycell = '&nbsp;';
	    $table = new \flexible_table('mod-scorm-report');
	    //print_object($data['columns']);print_object($data['headers']);
	    $table->define_columns($data['columns']);
	    $table->define_headers($data['headers']);
	    $table->define_baseurl($PAGE->url);

	    $table->sortable(true);
	    $table->collapsible(true);

	    // This is done to prevent redundant data, when a user has multiple attempts.
	    //$table->column_suppress('picture');
	    $table->column_suppress('fullname');
	    
	    foreach ($data['nosort'] as $field) {
	        $table->no_sorting($field);
	    }

	    $table->no_sorting('start');
	    $table->no_sorting('score');

	    foreach ($data['scoes'] as $sco) {
	        if ($sco->launch != '') {
	            $table->no_sorting('scograde'.$sco->id);
	        }
	    }

	    $table->column_class('fullname', 'bold');
	    $table->column_class('score', 'bold');

	    $table->set_attribute('cellspacing', '0');
	    $table->set_attribute('id', 'attempts');
	    $table->set_attribute('class', 'generaltable generalbox');

	    // Start working -- this is necessary as soon as the niceties are over.
	    $table->setup();
	    
	    if (!$download) {
	        $sort = $table->get_sql_sort();
	    } else {
	        $sort = '';
	    }
	    // Fix some wired sorting.
	    if (empty($sort)) {
	        $sort = ' ORDER BY uniqueid';
	    } else {
	        $sort = ' ORDER BY '.$sort;
	    }

	    if (!$download) {
	        // Add extra limits due to initials bar.
	        list($twhere, $tparams) = $table->get_sql_where();
	        if ($twhere) {
	            $where .= ' AND '.$twhere; // Initial bar.
	            $data['params'] = array_merge($data['params'], $tparams);
	        }

	        if (!empty($data['countsql'])) {
	            $count = $DB->get_record_sql($data['countsql'], $data['params']);
	            $totalinitials = $count->nbresults;
	            if ($twhere) {
	                $data['countsql'] .= ' AND '.$twhere;
	            }
	            $count = $DB->get_record_sql($data['countsql'], $data['params']);
	            $total  = $count->nbresults;
	        }

	        //$table->pagesize($pagesize, $total);

	        /*echo \html_writer::start_div('scormattemptcounts');
	        if ( $count->nbresults == $count->nbattempts ) {
	            echo get_string('reportcountattempts', 'scorm', $count);
	        } else if ( $count->nbattempts > 0 ) {
	            echo get_string('reportcountallattempts', 'scorm', $count);
	        } else {
	            echo $count->nbusers.' '.get_string('users');
	        }
	        echo \html_writer::end_div();*/
	    }
	    
	    $candelete=array();
	    // Fetch the attempts.
	    if (!$download) {
	        $attempts = $DB->get_records_sql($data['select'].$data['from'].$data['where'].$sort, $data['params'],
	        $table->get_page_start(), $table->get_page_size());
	        echo \html_writer::start_div('', array('id' => 'scormtablecontainer'));
	        
	        $table->initialbars($totalinitials > 20); // Build table rows.
	    } else {
	        $attempts = $DB->get_records_sql($data['select'].$data['from'].$data['where'].$sort, $data['params']);
	    }
	    
	    if ($attempts) {
	        foreach ($attempts as $scouser) {
	            $row = array();
	            if (!empty($scouser->attempt)) {
	                $timetracks = scorm_get_sco_runtime($scorm->id, false, $scouser->userid, $scouser->attempt);
	            } else {
	                $timetracks = '';
	            }
	            
	            if (!$download) {
	                $url = new \moodle_url('/user/view.php', array('id' => $scouser->userid, 'course' => $course->id));
	                $row[] = \html_writer::link($url, fullname($scouser));
	            } else {
	                $row[] = fullname($scouser);
	            }
	           
	            if (empty($timetracks->start)) {
	                $row[] = '-';
	                $row[] = '-';//for last was after this
	                $row[] = '-';
	            } else {
	                $row[] = $scouser->attempt;//link for the attempt on scorm
	                $row[] = userdate($timetracks->start);
	                $row[] = scorm_grade_user_attempt($scorm, $scouser->userid, $scouser->attempt);//show final score
	            }
	            // Print out all scores of attempt.
	            //echo "scoes";
	            //print_object($data['scoes']);
	            foreach ($data['scoes'] as $sco) {
	                if ($sco->launch != '') {
	                	//echo "scoes";
	                	//print_object($sco);
	                    if ($trackdata = scorm_get_tracks($sco->id, $scouser->userid, $scouser->attempt)) {
	                    	//echo "-3-";//print_object($trackdata);
	                        if ($trackdata->status == '') {
	                            $trackdata->status = 'notattempted';
	                        }
	                        $strstatus = get_string($trackdata->status, 'scorm');
	                        
	                        if ($trackdata->score_raw != '') { // If raw score exists, print it.
	                            $score = $trackdata->score_raw;
	                            // Add max score if it exists.
	                            if (isset($trackdata->score_max)) {
	                                $score .= '/'.$trackdata->score_max;
	                            }

	                        } else { // ...else print out status.
	                            $score = $strstatus;
	                        }
	                       	
                            $url = new \moodle_url('/mod/scorm/report/userreporttracks.php', array('id' => $contextCourse->id,
                                'scoid' => $sco->id, 'user' => $scouser->userid, 'attempt' => $scouser->attempt));
                                $row[] = \html_writer::img($OUTPUT->pix_url($trackdata->status, 'scorm'), $strstatus,
                                array('title' => $strstatus)) . \html_writer::empty_tag('br') .$score;
	                        
	                        // Iterate over tracks and match objective id against values.
	                        $keywords = array("cmi.objectives_", ".id");
	                        $objectivestatus = array();
	                        $objectivescore = array();
	                        foreach ($trackdata as $name => $value) {
	                            if (strpos($name, 'cmi.objectives_') === 0 && strrpos($name, '.id') !== false) {
	                                $num = trim(str_ireplace($keywords, '', $name));
	                                if (is_numeric($num)) {
	                                	//print_object($data['columns']);print_object($data['headers']);
	                                	//echo "<br>AA";
	                                    if (scorm_version_check($scorm->version, SCORM_13)) {
	                                        $element = 'cmi.objectives_'.$num.'.completion_status';
	                                        //echo "-A<br>";
	                                    } else {
	                                        $element = 'cmi.objectives_'.$num.'.status';
	                                        //echo "-A<br>";
	                                    }
	                                    if (isset($trackdata->$element)) {
	                                        $objectivestatus[$value] = $trackdata->$element;
	                                        //echo "-b<br>";
	                                    } else {
	                                        $objectivestatus[$value] = '';
	                                        //echo "-b<br>";
	                                    }
	                                    if ($displayoptions['objectivescore']) {
	                                        $element = 'cmi.objectives_'.$num.'.score.raw';
	                                        if (isset($trackdata->$element)) {
	                                            $objectivescore[$value] = $trackdata->$element;
	                                            //echo "-c<br>";
	                                        } else {
	                                            $objectivescore[$value] = '';
	                                            //echo "-c<br>";
	                                        }
	                                    }else{
	                                    	//echo "?";
	                                    }
	                                    //$row[] = $emptycell;
	                                }else{
	                                	//echo "<br>BB<br>";
	                                }
	                            }else{
	                            	//echo "<br>CC<br>";
	                            }
	                        }

	                        // Interaction data.
	                        if (!empty($data['objectives'][$trackdata->scoid])) {
	                        	//echo 'TEM';
	                            foreach ($data['objectives'][$trackdata->scoid] as $name) {
	                                /*if (isset($objectivestatus[$name])) {
	                                	//echo "<br>A<br>";
	                                    $row[] = s($objectivestatus[$name]);
	                                } else {
	                                	//echo "<br>B<br>";
	                                    $row[] = $emptycell;
	                                }*/
	                                if ($displayoptions['objectivescore']) {
	                                    if (isset($objectivescore[$name])) {
	                                    	//echo "<br>C<br>";
	                                        $row[] = s($objectivescore[$name]);
	                                    } else {
	                                    	//echo "<br>D<br>";
	                                        $row[] = $emptycell;
	                                    }

	                                }
	                            }
	                        }else{
	                        	//echo "no1";
	                        }
	                        //echo "-3.3-";
	                        // End of interaction data.
	                    } else {
	                        // If we don't have track data, we haven't attempted yet.
	                        //echo "If we don't have track data, we haven't attempted yet.";
	                        //echo "-4-";
	                        $strstatus = get_string('notattempted', 'scorm');
	                        if (!$download) {
	                            $row[] = \html_writer::img($OUTPUT->pix_url('notattempted', 'scorm'), $strstatus,
	                                        array('title' => $strstatus)).\html_writer::empty_tag('br').$strstatus;
	                        } else {
	                            $row[] = $strstatus;
	                        }
	                        // Complete the empty cells.
	                        for ($i = 0; $i < count($data['columns']) - $data['nbmaincolumns']; $i++) {
	                            $row[] = $emptycell;
	                        }
	                    }
	                }
	            }
	            //print_object($row);
	           	$table->add_data($row);
 				//die;
	        }

	        if (!$download) {
	            $table->finish_output();
	          	echo \html_writer::end_div();
	            
	        }
	    } else {
	        if ($candelete && !$download) {
	            echo \html_writer::end_div();
	            echo \html_writer::end_tag('form');
	            $table->finish_output();
	        }
	        echo \html_writer::end_div();
	    }
	}

//die;

/*
echo $OUTPUT->box_start('generalbox boxaligncenter');
echo $OUTPUT->heading("All Attempts", 3);

$table->define_baseurl($PAGE->url);

//SET TABLE CLASSES
//$table->set_attribute('class', 'generaltable generalbox boxaligncenter');
//SET TABLE ID
$table->set_attribute('id', 'new-report-table1');

//table columns [columns]
$table->define_columns(array('id','pupil', 'score', 'attmpt', 'time','q1','q2','q3'));

//table headers [headers]
$table->define_headers(
        array(
                format_string('Pupil Id'),
                format_string('Pupil Name'),
                format_string('Final Score'),
                format_string('Attempts'),
                format_string('Time'),
                format_string('Q1'),
                format_string('Q2'), 
                format_string('Q3')
            )
        );

$table->setup();

//GET THIS COURSE STUDENTS
$query =   'SELECT u.id AS id, firstname, lastname 
			FROM mdl_role_assignments AS a, mdl_user AS u 
			WHERE contextid = '.$contextCourse->id.' AND roleid = 5 AND a.userid = u.id;';
$students = $DB->get_recordset_sql( $query ); 

foreach( $students as $studentId => $studentData) {
	$dataByRow = array(); 
	$dataByRow[] = $studentId;
	$dataByRow[] = $studentData->firstname;
	//$table->column_style_all('display', 'block');
	if($studentData->firstname=='Maria'){
		//$table->column_class[''] = 'result1';
		//$table->column_style('display', 'block');
		$dataByRow[] = '80';//    display: block;
		$dataByRow[] = '1';
		$dataByRow[] = '100s';
		$dataByRow[] = '10';
		$dataByRow[] = '0';
		$dataByRow[] = '5';	
	}else{
		$dataByRow[] = '50';
		$dataByRow[] = '1';
		$dataByRow[] = '50s';
		$dataByRow[] = '0';
		$dataByRow[] = '0';
		$dataByRow[] = '100';
	}
	
	$table->add_data($dataByRow);
}
//print_object($table);die;
$table->finish_output();

echo $OUTPUT->box_end();
echo $OUTPUT->footer();

*/