<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The gradebook scorm report
 *
 * @package   gradereport_scorm
 * @copyright 2007 Nicolas Connault
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later

 */

// File: /mod/mymodulename/view.php
require_once '../../../config.php';//has CFG variable
require_once $CFG->dirroot.'/grade/report/scorm/lib.php';
require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->dirroot.'/grade/lib.php');// has function: print_grade_page_head

// COURSE ID AS REQUIRED PARAMETER
$courseid = required_param('id', PARAM_INT);

// ACTIVITY ID OR STUDENT ID AS OPTIONAL, SO WE KNOW THE TABLE TO BUILD ACCORDING TO THE PARAMETER
$userid = optional_param('user', null, PARAM_INT); // User ID.
$activityID = optional_param('act', null, PARAM_INT); // Activity ID.
$attemptID = optional_param('attempt', 1, PARAM_INT); // Attempt ID. (gets first attempt by default)
$groupid  = optional_param('group', null, PARAM_INT); // Group ID.

//data for context
$course = $DB->get_record('course', array('id' => $courseid));
//require login - VALIDATION
require_login($course);

//check the layout used so we change the page content
$isIrishLayout = false;
if($PAGE->course->theme=='irishmedium'){
    $isIrishLayout = true;
}

// Set up the page.
$PAGE->set_url(new moodle_url('/grade/report/scorm/index.php', array('id'=>$courseid)));
// Context
$contextCourse = context_course::instance($courseid);

//class for CSS style #page-grade-report-scorm-index

if(!$isIrishLayout){
    /* WITH HEADER LIKE MOODLE */
    
    $PAGE->set_pagelayout('report'); // Layout

    // GATHER DATA FOR THE HEADER
    $reportname = get_string('pluginname', 'gradereport_scorm');
    // function that prints header/heading
    print_grade_page_head($courseid, 'report', 'scorm', $reportname);

}
else{ /* WITHOUT HEADER LIKE MOODLE */

    // Format header
    //echo $OUTPUT->header(); 
    // Format heading
    //echo $OUTPUT->heading(get_string('pluginname', 'gradereport_scorm'));//format_string('someName')
}

// ******************* BUILD DROPDOWN MENUS ******************* //

//get data for classes/groups
$classes = gradereport_scorm::getCourseClass($courseid);
//build select
$selectClass = gradereport_scorm::buildSelect($classes,'classid','Select Class');
//show select on the view
echo "$selectClass";

//get data for Units(Aonads)
$units = gradereport_scorm::getCourseFlexSection($courseid);
//build select
$selectUnit = gradereport_scorm::buildSelect($units,'unitid','Select Unit');
//show select on the view
echo "$selectUnit";

//get data for activities on the course (SCORM)
$activities = gradereport_scorm::getActivityClass($courseid);
//build select
$selectActv = gradereport_scorm::buildSelect($activities,'actvid','Select Activity');
//show select on the view
echo "$selectActv";

echo "<br><br>";

// ******************* BUILD REPORT TABLE ******************* //

// Define table columns.
$columns = array();
$headers = array();
$columns[] = 'fullname';
$headers[] = get_string('name');
$columns[] = 'attempt';
$headers[] = get_string('attempt', 'scorm');
$columns[] = 'status';
$headers[] = get_string('status', 'scorm');
$columns[] = 'score';
$headers[] = get_string('score', 'scorm');
$columns[] = 'start';
$headers[] = get_string('started', 'scorm');

$table = new \flexible_table('scorm-report-table');

$table->define_baseurl($PAGE->url);

echo \html_writer::start_div('', array('id' => 'scormtablecontainer'));

if(empty($activityID)){
    // GET THIS COURSE SCORM ACTIVITIES
    $courseScorms = gradereport_scorm::getScorms($courseid);
    
    // By SCORM ...s
    foreach ($courseScorms as $key => $scorm) {
        $activityID = $scorm->id;
        //$data = gradereport_scorm::getScormData2($students,$scorm,$contextCourse,$course);    
        //$data = gradereport_scorm::getRecords($students,$scorm);
    }
}

//FOR EACH SCORM
//get the number of questions for the current scorm
$questioncount = gradereport_scorm::getQuestionCount($activityID);
for ($i=1; $i < $questioncount+1; $i++) {
    $columns[] = 'Q'.$i;
    $headers[] = 'Q'.$i;
}

$table->define_columns($columns);
$table->define_headers($headers);

//$table->sortable(true);//remove capability of sorting things on the table
//$table->collapsible(true);//add capability of dock/hide columns (that little square)

// This is done to prevent redundant data, when a user has multiple attempts.
$table->column_suppress('fullname');//removes name when the attempt is the same

$table->column_class('fullname', 'bold');
//$table->column_class('score', 'bold');

//$table->set_attribute('cellspacing', '0');
$table->set_attribute('id', 'somerandomid');
$table->set_attribute('class', '');//generaltable generalbox

// Start working -- this is necessary as soon as the niceties are over.
$table->setup();

// CHOOSE METHOD ACCORDING TO SHOW ALL ATTEMPTS OF JUST ONE
if($attemptID=='all'){
    // GET THIS COURSE STUDENTS IDS
    $students = gradereport_scorm::getStudentsArray($contextCourse->id);
    // GATHER QUESTIONS AND ANSWERS BY STUDENT ID, scorm and attempt
    $allData = array();
    // FOR EACH STUDENT
    for ($x=0; $x < count($students); $x++) { 
        // restart variables
        $studentAttempts = array(); $byAttempt = array();
        
        // get the student id
        $studentId = $students[$x];
        // get the student name
        $thisStudentsData = gradereport_scorm::getStudentsObj($contextCourse->id,$studentId);
        foreach ($thisStudentsData as $thisStudentId => $studentData) {
            $byAttempt['name'] = $studentData->firstname.' '.$studentData->lastname;
        }
        
        // GET ALL ITS ATTEMPTS IDS
        $attemptids = scorm_get_all_attempts($activityID, $studentId);
        if(!empty($attemptids)){
            // FOR EACH ATTEMPT
            for ($j=0; $j < count($attemptids); $j++) {
                // restart variables
                $scormAttempt = array(); $scormData = array();
                // get current attempt
                $curAttempt = $attemptids[$j];
                $byAttempt['attempt'] = $curAttempt;
                // GATHER QUESTIONS AND ANSWERS
                $scormData = gradereport_scorm::getScormRecords($questioncount,$studentId,$activityID,$curAttempt);
                // add current attempt data to the current student data
                $scormAttempt = $byAttempt + $scormData;
                // add this data to the all students attempts
                $studentAttempts[] = $scormAttempt;
            }
        }else{
            // if there's no attempts just create the default values for the current student
            $default = array(
                            'attempt'=>'not attempted',
                            'status'=>'',
                            'score'=>''
                        );
            // add this to the current structure
            $scormAttempt = $byAttempt + $default;
            $studentAttempts[] = $scormAttempt;
        }
        // add all this student attempts to the structure
        $allData[] = $studentAttempts;
    }
    
    // build table
    for ($h=0; $h < count($allData); $h++) {
        // get current student
        $student = $allData[$h];
        for ($w=0; $w < count($student); $w++) {
            // get current attempt
            $studentAtt = $student[$w];
            $row = array();
            // add this current attempt data to the table
            foreach ($studentAtt as $key => $value) {
                //Add pupil's name
                if($key=='name'){
                    $row[] = $value;
                }else
                //Add attempt number
                if($key=='attempt'){
                    $row[] = $value;
                }else
                //Add activity final status
                if($key=='status'){
                    if(empty($value)){
                        $row[] = '-';
                    }else{
                        $row[] = $value;
                    }
                }else
                //Add activity final score
                if($key=='score'){
                    $row[] = $value;//will be empty if no answer
                }else
                //Add start date
                if($key=='time'){
                    $row[] = $value;
                }else
                //Add answers 
                if(!empty($value)){
                    //CHECK IF ITS WITH HINTS
                    if(strpos($value, "->")>0){
                        //IF SO, CLEAN STRING TO SHOW WITH ICON AND HINT NUMBER
                        $removeArrowLeft = str_replace('->', '', $value);
                        //get final state and clean hints
                        if(strpos($value, "wrong")===0){
                            $finalSate = 'failed';
                            $hints = str_replace("wrong", '', $removeArrowLeft);
                        }elseif(strpos($value, "correct")===0){
                            $finalSate = 'completed';
                            $hints = str_replace("correct", '', $removeArrowLeft);
                        }
                        //get hints number
                        $hintsNumb = str_replace("<-", '', $hints);
                        //build with icon 
                        $row[] = \html_writer::img($OUTPUT->pix_url($finalSate, 'scorm'),$finalSate,
                        array('title' => $finalSate)) . \html_writer::empty_tag('br').$hintsNumb;

                    }else{
                        //clean string to get final state
                        $value = str_replace('--', '', $value);
                        //if not with hint, we just show final state with icon wright or wrong
                        $finalSate = ( $value=='correct' ? 'completed' : 'failed');
                        $row[] = \html_writer::img($OUTPUT->pix_url($finalSate, 'scorm'),$finalSate,
                        array('title' => $finalSate)) . \html_writer::empty_tag('br');
                    }
                }
            }
            // ADD MISS ROWS
            if(count($row)<count($columns)){
                $missRows = count($columns)-count($row);
                for ($i3=0; $i3 < $missRows; $i3++) { 
                    $row[] = '-';
                }
            }
            $table->add_data($row);
        }
    }
}else{
    $activityData = array();
    // GET THIS COURSE STUDENTS
    $students = array();
    if(empty($userid) && empty($groupid)){
        $students = gradereport_scorm::getStudentsArray($contextCourse->id);

        // GATHER QUESTIONS AND ANSWERS BY STUDENT ID, scorm and attempt
        for ($i=0; $i <count($students) ; $i++) { 
            $studentId = $students[$i];
            $data = gradereport_scorm::getScormRecords($questioncount,$studentId,$activityID,$attemptID);
            $activityData[$studentId] = $data;
        }
        
    }elseif(!empty($groupid)){
        $students = gradereport_scorm::getStudentsGroups($groupid);

        // GATHER QUESTIONS AND ANSWERS BY STUDENT ID, scorm and attempt
        for ($i=0; $i <count($students) ; $i++) { 
            $studentId = $students[$i];
            $data = gradereport_scorm::getScormRecords($questioncount,$studentId,$activityID,$attemptID);
            $activityData[$studentId] = $data;
        }

    }else{
        $students[] = $userid;

        $data = gradereport_scorm::getScormRecords($questioncount,$userid,$activityID,$attemptID);
        $activityData[$userid] = $data;
    }

    //Build query and cicle that according to the parameters received it will show a table

    // Used when an empty cell is being printed - in html we add a space.
    $emptycell = '&nbsp;';
    $students2 = gradereport_scorm::getStudentsObj($contextCourse->id,$students);
    foreach ($students2 as $key2 => $value) {
        $row = array();
        $row[] = $value->firstname.' '.$value->lastname;
        $row[] = "$attemptID";
        foreach ($activityData[$key2] as $question => $answer) {
            if(!empty($answer)){
                //$row[] = $answer;

                //SHOW FINAL STATUS OF THE ACTIVITY
                if($question=='status'){
                    $row[] = $answer;
                }
                //FOR FINAL SCORE OF THE ACTIVITY
                elseif ($question=='score') {
                    
                    $row[] = $answer;
                }
                //FOR FINAL SCORE OF THE ACTIVITY
                elseif ($question=='time') {
                    $row[] = $answer;
                }
                //CHECK IF ITS WITH HINTS
                elseif(strpos($answer, "->")>0){
                    //IF SO, CLEAN STRING TO SHOW WITH ICON AND HINT NUMBER
                    $removeArrowLeft = str_replace('->', '', $answer);
                    //get final state and clean hints
                    if(strpos($answer, "wrong")===0){
                        $finalSate = 'failed';
                        $hints = str_replace("wrong", '', $removeArrowLeft);
                    }elseif(strpos($answer, "correct")===0){
                        $finalSate = 'completed';
                        $hints = str_replace("correct", '', $removeArrowLeft);
                    }
                    //get hints number
                    $hintsNumb = str_replace("<-", '', $hints);
                    //build with icon 
                    $row[] = \html_writer::img($OUTPUT->pix_url($finalSate, 'scorm'),$finalSate,
                    array('title' => $finalSate)) . \html_writer::empty_tag('br').$hintsNumb;

                }else{
                    //clean string to get final state
                    $answer = str_replace('--', '', $answer);
                    //if not with hint, we just show final state with icon wright or wrong
                    $finalSate = ( $answer=='correct' ? 'completed' : 'failed');
                    $row[] = \html_writer::img($OUTPUT->pix_url($finalSate, 'scorm'),$finalSate,
                    array('title' => $finalSate)) . \html_writer::empty_tag('br');
                }
            }else{
                if ($question=='score') {
                    $row[] = $answer;
                }else{
                    $row[] = '-';
                }
            }
        }
        // ADD MISS ROWS
        if(count($row)<count($columns)){
            $missRows = count($columns)-count($row);
            for ($i3=0; $i3 < $missRows; $i3++) { 
                //$row[] = $emptycell;
                $row[] = '-';
            }
        }
        $table->add_data($row);
    }
}

/* WITH FOOTER LIKE MOODLE */
if(!$isIrishLayout){
    // FINISH BUILD TABLE
    $table->print_html(); // $table->finish_output();
    // end scorm div
    echo \html_writer::end_div();
    // print footer
    echo $OUTPUT->footer();
}