<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Base lib class for scorm functionality.
 *
 * @package   gradereport_nscorm * @copyright 2014 Moodle Pty Ltd (http://moodle.com)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/grade/report/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->dirroot.'/mod/scorm/report/reportlib.php');

/**
 * This class is the main class that must be implemented by a grade report plugin.
 *
 * @package   gradereport_scorm
 * @copyright 2014 Moodle Pty Ltd (http://moodle.com)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class gradereport_scorm extends grade_report {

    /**
     * Return the list of valid screens, used to validate the input.
     *
     * @return array List of screens.
     */
    public static function valid_screens() {
        // This is a list of all the known classes representing a screen in this plugin.
        return array('user', 'select', 'grade');
    }

    /**
     * Process data from a form submission. Delegated to the current screen.
     *
     * @param array $data The data from the form
     * @return array List of warnings
     */
    public function process_data($data) {
        if (has_capability('moodle/grade:edit', $this->context)) {
            return $this->screen->process($data);
        }
    }

    /**
     * Unused - abstract function declared in the parent class.
     *
     * @param string $target
     * @param string $action
     */
    public function process_action($target, $action) {
    }

    /**
     * Constructor for this report. Creates the appropriate screen class based on itemtype.
     *
     * @param int $courseid The course id.
     * @param object $gpr grade plugin return tracking object
     * @param context_course $context
     * @param string $itemtype Should be user, select or grade
     * @param int $itemid The id of the user or grade item
     * @param string $unused Used to be group id but that was removed and this is now unused.
     */
    public function __construct($courseid, $gpr, $context, $itemtype, $itemid, $unused = null) {
        parent::__construct($courseid, $gpr, $context);

        $base = '/grade/report/scorm/index.php';

        $idparams = array('id' => $courseid);

        $this->baseurl = new moodle_url($base, $idparams);

        $this->pbarurl = new moodle_url($base, $idparams + array(
                'item' => $itemtype,
                'itemid' => $itemid
            ));

        //  The setup_group method is used to validate group mode and permissions and define the currentgroup value.
        $this->setup_groups();

        $screenclass = "\\gradereport_scorm\\local\\screen\\${itemtype}";

        $this->screen = new $screenclass($courseid, $itemid, $this->currentgroup);

        // Load custom or predifined js.
        $this->screen->js();
    }

    /**
     * Build the html for the screen.
     * @return string HTML to display
     */
    public function output() {
        global $OUTPUT;
        return $OUTPUT->box($this->screen->html());
    }

    //------------------------------------------------------------ NEW FUNCTIONS TO GET DATA FOR THE REPORT

    // function used to get the groups of the course
    public static function getCourseClass($courseid) {       
        global  $DB;
        
        //get classes
        $cquery =   'SELECT id, name  
                    FROM mdl_groups as c
                    WHERE courseid = '.$courseid.';';
        $classesData = $DB->get_recordset_sql( $cquery ); 

        $classes = array();
        foreach ($classesData as $key => $class) {
            $classes[] = array('id'=>$class->id,'name'=>$class->name);
        }

      return $classes;
    }

    // function used to get the sections of the course (not used now because of flexpage)
    public static function getCourseSection($courseid) {       
        global  $DB;
        
        //the first record doesnt have name because its the same for all courses - a first section where the news forum is, so should we include that?
        $sectionsData = $DB->get_records('course_sections', array('course' => $courseid) , '', 'id, name');

        $sections = array();
        foreach ($sectionsData as $key => $sect) {
            $sections[] = array('id'=>$sect->id,'name'=>$sect->name);
        }

      return $sections;
    }

    // function used to get the (flex) sections of the course
    public static function getCourseFlexSection($courseid) {       
        global  $DB;
        
        $unitsquery = 'SELECT id, name
            FROM mdl_format_flexpage_page as c
            WHERE courseid = '.$courseid.' and name like "Aonad%";';
        $flex_units = $DB->get_recordset_sql($unitsquery); 
        
        // Obtain a list of units
        foreach ($flex_units as $key => $row) {
            $units[$key] = $row->name;
        }
        // Sort by "natural ordering"
        natsort($units);

        // Gather data for select
        $data = array();
        foreach ($units as $key => $value) {
            $data[] = array('id'=>$key,'name'=>$value);
        }
        return $data;
    }

    // function used to get all the scorm type activities (id, name)
    public static function getActivityClass($courseid) {       
        global  $DB;
        
        //get activ
        $cquery =   'SELECT 
                        cm.id, 
                        cm.module, 
                        m.name 
                    FROM 
                        mdl_course_modules cm 
                        LEFT JOIN mdl_modules m on m.id = cm.module 
                    WHERE 
                        cm.course = '.$courseid.';';
        $activData = $DB->get_recordset_sql( $cquery ); 

        $ScormAct = array();
        foreach ($activData as $key => $class) {
            $activType = $class->name;
            switch ($activType) {
        case 'scorm':
            $ScormAct[] = array('id'=>$class->id,'name'=>$class->name);
            break;
        
        default:
            # code...
            break;
            }
        }

        $scorms = array();
        if(!empty($ScormAct)){
            //get scorm names
            $scquery =   'SELECT id, name FROM  mdl_scorm as c 
                            WHERE course = '.$courseid.';';
            $scoms = $DB->get_recordset_sql( $scquery ); 
            foreach ($scoms as $key => $class) {
                $scorms[] = array('id'=>$class->id,'name'=>$class->name);
            }
        }else{
            echo "<br><br>This course doesn't have scorm type activities<br><br>";
        }
      return $scorms;
    }

    // function used to build a select with similar properties to the Moodle ones
    public static function buildSelect($data,$name,$selectWhat){
        $beginSelect = '<select id="" class="select autosubmit singleselect" name="'.$name.'">';
        $endSelect = '</select>';
        //build select content
        $select = '';
        if(!empty($data)){
            $selectOption='<option selected="selected" value="">'.$selectWhat.'</option>';
            $selectContent='';
            for ($i=0; $i <count($data) ; $i++) { 
        $selectContent.='<option value="'.$data[$i]['id'].'">'.$data[$i]['name'].'</option>';
            }
            $select = $beginSelect.$selectOption.$selectContent.$endSelect;
        }
        return $select;
    }

    /**
     * Returns the full list of attempts a user has made.
     *
     * @param int $scormid the id of the scorm.
     * @param int $userid the id of the user.
     *
     * @return array array of attemptids
     */
    public function scorm_get_all_attempts($scormid, $userid) {
        global $DB;
        $attemptids = array();
        $sql = "SELECT DISTINCT attempt FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? ORDER BY attempt";
        $attempts = $DB->get_records_sql($sql, array($userid, $scormid));
        foreach ($attempts as $attempt) {
            $attemptids[] = $attempt->attempt;
        }
        return $attemptids;
    }

    // function used to get all the scorm type activities within a course
    public static function getScorms($courseid){
        global  $DB;
        //get scorm names
        $scquery =   'SELECT id, name, version, grademethod 
                    FROM mdl_scorm as c
                    WHERE course = '.$courseid.';';
        $scorms = $DB->get_recordset_sql($scquery);
        return $scorms;
    }

    /* FUNCTION USED TO KNOW THE NUMBER OF QUESTIONS THAT SOME SCORM HAS */
    public static function getQuestionCount($scormID){
        //get the number of questions for the current scorm
        $questioncount = get_scorm_question_count($scormID);
        return $questioncount;
    }

    /* get the results for each attempt, user and activity */
    public static function getScormRecords($questioncount,$studentID,$scormID,$attemptID){
        global  $DB;
        
        //get records by student, scorm and attempt
        $trackdata = $DB->get_records('scorm_scoes_track', array('userid' => $studentID, 'scormid' => $scormID,
            'attempt' => $attemptID));
        
        $usertrack = scorm_format_interactions($trackdata);
        
        //By question, grab the answer
        $allData = array();
        for ($i = 0; $i < $questioncount; $i++) {
            $question =''; $answer ='';
            
            //get each element (questions and answer)
            $element = 'cmi.interactions_'.$i.'.id';
            if (isset($usertrack->$element)) {
        $question = s($usertrack->$element);
        
        $element = 'cmi.interactions_'.$i.'.result';
        if (isset($usertrack->$element)) {
            $answer = s($usertrack->$element);
        } else {
            $answer = '&nbsp;';
        }

        $element = 'cmi.interactions_'.$i.'.student_response';
        if (isset($usertrack->$element)) {
            $answer.= '->'.$usertrack->$element.'<-';
        } else {
            $answer.= '--';
        }
            }
            //get status
            $status = 'status';
            if (isset($usertrack->$status)) {
        if(!isset($allData['status'])){
            $allData['status']=s($usertrack->$status);
        }
            }
            //get score
            $score = 'score_raw';
            if (isset($usertrack->$score)) {
        if(!isset($allData['score'])){
            $allData['score']=s($usertrack->$score);
        }
            }
            //get start time
            $time = 'x.start.time';
            if (isset($usertrack->$time)) {
        if(!isset($allData['time'])){
            $allData['time'] = date('d/m/Y H:i', $usertrack->$time);
        }
            }
            $allData[$question] = $answer;
        }
        return $allData;
    }
    
    // Get the students' IDS from the current course 
    public static function getStudentsArray($contextCourseId){
        global $DB;

        $query = 'SELECT u.id AS id
        FROM mdl_role_assignments AS a, mdl_user AS u 
        WHERE contextid = '.$contextCourseId.' AND roleid = 5 AND a.userid = u.id;';
        $students = $DB->get_recordset_sql($query);

        $allowedlist = array();
        foreach( $students as $studentId => $studentData) {
            $allowedlist[] = $studentId;
        }
        return $allowedlist;
    }

    // Get the students' IDS from the current group 
    public static function getStudentsGroups($groupid){
        global $DB;

        $query = 'SELECT userid 
        FROM mdl_groups_members
        WHERE groupid = '.$groupid;
        $students = $DB->get_recordset_sql($query);

        $allowedlist = array();
        foreach($students as $studentId) {
            $allowedlist[] = $studentId->userid;
        }
        return $allowedlist;
    }

    // Get student's data (object) from the current course (or limit by user id)
    public static function getStudentsObj($contextCourseId,$userid=''){
        global $DB;
        
        $limit ='';
        if(!empty($userid)){
            //decide wether to use implod if the parameter received is an array of not
            $limit ='AND a.userid in ('.(is_array($userid) ? implode(',', $userid) : $userid).')';
        }
        
        $query = 'SELECT u.id AS id, firstname, lastname 
        FROM mdl_role_assignments AS a, mdl_user AS u 
        WHERE contextid = '.$contextCourseId.' AND roleid = 5 AND a.userid = u.id '.$limit.'
        ORDER BY firstname ASC;';
        $students = $DB->get_recordset_sql($query);
        return $students;
    }
}
