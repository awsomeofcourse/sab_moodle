<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * This page displays the user data from a single attempt
 *
 * @package mod_scorm
 * @copyright 1999 onwards Martin Dougiamas {@link http://moodle.com}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->libdir.'/tablelib.php');

$id = required_param('id', PARAM_INT); // Course Module ID.
$userid = required_param('user', PARAM_INT); // User ID.
$scoid = required_param('scoid', PARAM_INT); // SCO ID.
$attempt = optional_param('attempt', 1, PARAM_INT); // attempt number.*/
//$download = optional_param('download', '', PARAM_ALPHA);


// Checking login +logging +getting context.
$cm = get_coursemodule_from_id('scorm', $id, 0, false, MUST_EXIST);
$course = get_course($cm->course);
//$PAGE->set_context(get_system_context()); //deprecated
require_login($course, false, $cm);
$contextmodule = context_module::instance($cm->id);
require_capability('mod/scorm:viewreport', $contextmodule);

//SET THE PAGE LAYOUT (already has the blocks used)
$PAGE->set_pagelayout('standard');//or admin or standard....
//SET THE PAGE'S TITLE AND HEADER
$PAGE->set_title("My First Page");
$PAGE->set_heading("My Moodle's Page");

// Building the url to use for links (only after I did this, I saw the "Hello world")
/*$url = new moodle_url('/mod/scorm/report/userreporttracksv6.php', array('id' => $id,
    'user' => $userid,
    'attempt' => $attempt,
    'scoid' => $scoid));*/
$PAGE->set_url('/mod/scorm/report/userreporttracksv7.1.php');
// END of url setting

//GET DATA FROM DB
$user = $DB->get_record('user', array('id' => $userid), user_picture::fields(), MUST_EXIST);
$selsco = $DB->get_record('scorm_scoes', array('id' => $scoid), '*', MUST_EXIST);

$tracks = $DB->get_records('scorm_scoes_track', array('userid' => $userid, 'scoid' => $scoid,
                                                          'attempt' => $attempt), '','*');

   

    $new = false;
    if($cm->name=='Spanish'){
        $new = true;
    }

    //BEGIN TO PROCESS DATA RECEIVED
    $curr_id = 0;
    $group = array();
    $other = array();
    
    // Distinguish parameters from Puzzle or Technology Activity
    foreach ($tracks as $key => $obj) {
        $has_= false;
        if (stristr($obj->element, 'cmi.interactions_') == true) {
            $has_= true;break;
        }
    }

    // Here I'm going to group the records for each question
    if ($has_) {
        // Puzzle Activity - echo "Puzzle Activity";
        foreach ($tracks as $record => $obj) {
            //Swith between Interactions, Objectives or other recods
            $curElement = stristr(str_replace('cmi.', '', $obj->element), '_',true);
            switch ($curElement) {
                
                case 'interactions':
                    
                    //get the ID and Name of the current element
                    $elemntIdName = str_replace(strstr($obj->element, '_',true).'_', '', $obj->element);
                    //get only the ID
                    $interaction_id = strstr($elemntIdName, '.',true);
                    //for each different question -> create a new group
                    $group[$interaction_id]['question_id'] = $interaction_id;
                    //get the name, to add to the group
                    $interaction_name = str_replace('.', '', strstr($elemntIdName, '.'));
                    //Compare to the current one to check if we can add to the same group
                    if ($curr_id==$interaction_id) {
                        //add to the current group
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }else{
                        //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                        if($interaction_name=='id'){
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }
                    }

                    //Update current id to be compared
                    $curr_id = $interaction_id;
                    
                    break;

                case 'objectives':

                    $objectives = str_replace('cmi.objectives_', '', $obj->element);
                    //get only the ID
                    $interaction_id = str_replace('cmi.objectives_', '', $obj->element);
                    $interaction_id = strstr(str_replace('cmi.objectives_', '', $obj->element), '.',true);

                    //get the name, to add to the group
                    $interaction_name = str_replace('cmi.objectives_'.$interaction_id.'.', '', $obj->element);

                    //Compare to the current one to check if we can add to the same group
                    if ($curr_id==$interaction_id) {
                        //add to the current group
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }else{
                        //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                        if($interaction_name=='score.raw'){
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }
                    }
                    //Update current id to be compared
                    $curr_id = $interaction_id;

                    break;
                
                default:
                    
                    if (stristr($obj->element, 'core') == true) {
                        $otherName = str_replace('cmi.core.', '', $obj->element);
                        $other[$otherName] = $obj->value;
                    }else{
                        $date = date('d/m/Y H:i:s', $obj->value);
                        $other[$obj->element] = $obj->value.' -> '.$date;
                    }
                    break;
            }
        }
    }
    else{
        //echo "Technology Activity";
        foreach ($tracks as $record => $obj) {

            //Swith between Interactions, Objectives or other recods
            $piece =str_replace('cmi.', '', $obj->element);
            $curElement = stristr($piece, '.',true);
            
            if (stristr($obj->element, 'cmi.interactions') !== false) {
                //get the ID and Name of the current element
                $elemntIdName = str_replace('cmi.interactions.', '',$obj->element);
                //get only the ID
                $interaction_id = strstr($elemntIdName, '.',true);
                //for each different question -> create a new group
                $group[$interaction_id]['question_id'] = $interaction_id;

                //Ignore the elements that don't match to what we are looking for
                if(stristr($obj->element, '.objectives.') !== false || stristr($obj->element, '.correct_responses.') !== false){

                    //get the first name
                    $interaction_1name = strstr(str_replace(strstr($elemntIdName, '.',true).'.', '',  $elemntIdName), '.',true);
                    //get the second name
                    $interaction_2name = str_replace('0.', '', strstr($obj->element, '0.'));
                    //its an exception for the case when the the id changes
                    if(stristr($interaction_2name, '.') !== false){
                        $interaction_2name = str_replace($interaction_1name.'.', '', $interaction_2name);
                    }
                    //add the two names, on a single string
                    $interaction_name = $interaction_1name.'_'.$interaction_2name;
                }
                else{
                    //get the name, to add to the group
                    $interaction_name = str_replace('.', '', strstr($elemntIdName, '.'));
                }
                
                //Compare to the current one to check if we can add to the same group
                if ($curr_id==$interaction_id) {
                    //add to the current group
                    $group[$interaction_id][$interaction_name] = $obj->value;
                }else{
                    //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                    if($interaction_name=='objectives_id'){
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }
                }

                //Update current id to be compared
                $curr_id = $interaction_id;
            }
            else{
                if (stristr($obj->element, 'cmi.') == true) {
                    $field = str_replace('cmi.','', $obj->element);
                    $other[$field] = $obj->value;
                    if(stristr($obj->element, '_time') == true){
                        $other[$field] = scorm_format_duration($obj->value);
                    }
                }else{
                    $date = date('d/m/Y H:i:s', $obj->value);
                    $other[$obj->element] = $obj->value.' -> '.$date;
                }
            }
        }
    }

    //START SHOWING CONTENT ON THE PAGE
    echo $OUTPUT->header();

    $table = new flexible_table('mod_scorm-userreporttracks2');//[uniqueid]

    echo $OUTPUT->heading(format_string('scorm name'));
    $currenttab = '';
    //require($CFG->dirroot . '/mod/scorm/report/userreporttabs.php');
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->heading("attempt 1 - ". fullname($user).': '.format_string($selsco->title). ' - '. get_string('details', 'scorm'), 3);

    $table->define_baseurl($PAGE->url);

    //SET TABLE CLASSES
    //$table->set_attribute('class', 'generaltable generalbox boxaligncenter');
    //SET TABLE ID
    $table->set_attribute('id', 'mod_scorm-userreporttracks2');

    $line = '';$line2 = '';
    if($has_){
        
        // Maths Activity
        if($new){
            //table columns [columns]
            $table->define_columns(array('question_id', 'id', 'c_response','s_response','result','type'));
           
            //table headers [headers] - the matching names are on the file scorm.php
            $table->define_headers(
                    array(
                            get_string('question', 'scorm'), 
                            get_string('id', 'scorm'),
                            get_string('cresponse', 'scorm'),
                            get_string('sresponse', 'scorm'),
                            get_string('result', 'scorm'),
                            get_string('type', 'scorm')
                            )
                    );
            
            $table->setup();
            foreach ($group as $key33 => $value33) {
                $row2 = array();
                //add columns values
                $row2[] = $value33['question_id'];
                $row2[] = $value33['id'];
                $row2[] = $value33['correct_responses_0pattern'];
                $row2[] = $value33['student_response'];
                $row2[] = '';//$value33['result']
                if($value33['result']=='wrong'){
                    $table->column_class['result'] = 'result1';
                }else{
                    $table->column_class['result'] = 'result2';
                }
                $row2[] = $value33['type'];
                $table->add_data($row2);
            }
        }else{
            // Puzzle Activity

            //table columns [columns]
            $table->define_columns(array('question_id', 'id','type','time','result','score.raw'));
           
            //table headers [headers] - the matching names are on the file scorm.php
            $table->define_headers(
                    array(  get_string('question', 'scorm'), 
                            get_string('id', 'scorm'),
                            get_string('type', 'scorm'),
                            get_string('time', 'scorm'),
                            get_string('result', 'scorm'),
                            get_string('score.raw', 'scorm'))
            );

            $table->setup();
            foreach ($group as $key33 => $value33) {
                $row2 = array();
                //add columns values
                $row2[] = $value33['question_id'];
                $row2[] = $value33['id'];
                $row2[] = $value33['type'];
                $row2[] = $value33['time'];
                $row2[] = '';//$value33['result']
                if($value33['result']==0){
                    $table->column_class['result'] = 'result1';
                }else{
                    $table->column_class['result'] = 'result2';
                }
                $row2[] = $value33['score.raw'];
                $table->add_data($row2);
            }
        }
    }else{
        // Technology Activity or Human Eye

        if(count($group)>0){
            // build table content - Interactions + Objectives
            for ($i=0; $i < count($group) ; $i++) {
                $line.= "<tr><td>".$group[$i]['question_id']."</td><td>".$group[$i]['correct_responses_pattern']."</td><td>".$group[$i]['description']."</td><td>".$group[$i]['id']."</td><td>".$group[$i]['learner_response']."</td><td>".$group[$i]['objectives_id']."</td><td>".$group[$i]['result']."</td><td>".$group[$i]['timestamp']."</td><td>".$group[$i]['type']."</td><td>".$group[$i]['weighting']."</td></tr>";
            }

            // show data on the table
            echo "<table>
                    <thead>
                        <tr><th>Question Id</th><th>Correct Response</th><th>Description</th><th>Id</th><th>Learner Response</th><th>Objectives Id</th><th>Result</th><th>Timestamp</th><th>Type</th><th>Weighting</th></tr>
                    </thead>
                    <tbody>
                        $line
                    </tbody>
                  </table>";
        }else{
            echo "Nothing to show<br/><br/>";
        }
    }  

$table->finish_output();
echo $OUTPUT->box_end();
echo $OUTPUT->footer();

/*by me - scorm 
#mod_scorm-userreporttracks2 tbody tr td.c4result2{
    background: blue;
}
#mod_scorm-userreporttracks2 tbody tr td.c4result1{
    background: white;
}
#mod_scorm-userreporttracks2{
    background: black;
}*/
