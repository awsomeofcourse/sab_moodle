<?php
// This file is part of SCORM trends report for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of graphs reporting plugin
 *
 * @package    scormreport_try
 * @copyright  2013 onwards Ankit Kumar Agarwal <ankit.agrr@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace scormreport_try;

defined('MOODLE_INTERNAL') || die();

/**
 * Main class for the trends report
 *
 * @package    scormreport_try
 * @copyright  2013 onwards Ankit Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class report extends \mod_scorm\report {
    /**
     * Displays the trends report
     *
     * @param \stdClass $id full SCORM object
     * @param \stdClass $userid - full course_module object
     * @param \stdClass $scoid - full course object
     * @param string $attempt - type of download being requested
     * @param string $download - type of download being requested
     * @return bool true on success
     */
     public function display($scorm, $cm, $course, $download) {
        echo "Nothing";die;
    //public function display($id, $userid, $scoid, $attempt, $download) {
        global $DB, $OUTPUT, $PAGE;
        // Checking login +logging +getting context.
        $cm = get_coursemodule_from_id('scorm', $id, 0, false, MUST_EXIST);
        $contextmodule = \context_module::instance($cm->id);
        //$scoes = $DB->get_records('scorm_scoes', array("scorm" => $scorm->id), 'id');

        // Groups are being used, Display a form to select current group.
        /*if ($groupmode = groups_get_activity_groupmode($cm)) {
                groups_print_activity_menu($cm, new \moodle_url($PAGE->url));
        }*/

        //GET DATA FROM DB
        $user = $DB->get_record('user', array('id' => $userid), user_picture::fields(), MUST_EXIST);
        $selsco = $DB->get_record('scorm_scoes', array('id' => $scoid), '*', MUST_EXIST);

        $tracks = $DB->get_records('scorm_scoes_track', array('userid' => $userid, 'scoid' => $scoid,
                                                                  'attempt' => $attempt), '','*');

        $new = false;
        if($cm->name=='Spanish'){
            $new = true;
        }

        //BEGIN TO PROCESS DATA RECEIVED
        $curr_id = 0;
        $group = array();
        $other = array();

        $table = new \flexible_table('mod-scorm-another-report');
        

        echo $OUTPUT->heading(format_string('scorm name'));
        //$sqlargs = array_merge($params, array($sco->id));
        //$attempts = $DB->get_records_sql($select.$from.$where, $sqlargs);
        // Determine maximum number to loop through.
        //$loop = self::get_sco_question_count($sco->id);

        $columns = array('question_id', 'id', 'c_response','s_response','result','type');
        $headers = array(
            get_string('question', 'scormreport_try'),
            get_string('id', 'scormreport_try'),
            get_string('cresponse', 'scormreport_try'),
            get_string('sresponse', 'scormreport_try'),
            get_string('result', 'scormreport_try'),
            get_string('type', 'scormreport_try'));


        $table->define_columns($columns);
        $table->define_headers($headers);
        $table->define_baseurl($PAGE->url);

        // Don't show repeated data.
        //$table->column_suppress('question');
        //$table->column_suppress('element');

        $table->setup();

        $line = '';$line2 = '';
        if($has_){
            
            // Maths Activity
            if($new){
                //table columns [columns]
                $table->define_columns(array('question_id', 'id', 'c_response','s_response','result','type'));
               
                //table headers [headers] - the matching names are on the file scorm.php
                $table->define_headers(
                        array(
                                get_string('question', 'scorm'), 
                                get_string('id', 'scorm'),
                                get_string('cresponse', 'scorm'),
                                get_string('sresponse', 'scorm'),
                                get_string('result', 'scorm'),
                                get_string('type', 'scorm')
                                )
                        );
                
                $table->setup();
                foreach ($group as $key33 => $value33) {
                    $row2 = array();
                    //add columns values
                    $row2[] = $value33['question_id'];
                    $row2[] = $value33['id'];
                    $row2[] = $value33['correct_responses_0pattern'];
                    $row2[] = $value33['student_response'];
                    $row2[] = '';//$value33['result']
                    if($value33['result']=='wrong'){
                        $table->column_class['result'] = 'result1';
                    }else{
                        $table->column_class['result'] = 'result2';
                    }
                    $row2[] = $value33['type'];
                    $table->add_data($row2);
                }
            }else{
                // Puzzle Activity

                //table columns [columns]
                $table->define_columns(array('question_id', 'id','type','time','result','score.raw'));
               
                //table headers [headers] - the matching names are on the file scorm.php
                $table->define_headers(
                        array(  get_string('question', 'scorm'), 
                                get_string('id', 'scorm'),
                                get_string('type', 'scorm'),
                                get_string('time', 'scorm'),
                                get_string('result', 'scorm'),
                                get_string('score.raw', 'scorm'))
                );

                $table->setup();
                foreach ($group as $key33 => $value33) {
                    $row2 = array();
                    //add columns values
                    $row2[] = $value33['question_id'];
                    $row2[] = $value33['id'];
                    $row2[] = $value33['type'];
                    $row2[] = $value33['time'];
                    $row2[] = '';//$value33['result']
                    if($value33['result']==0){
                        $table->column_class['result'] = 'result1';
                    }else{
                        $table->column_class['result'] = 'result2';
                    }
                    $row2[] = $value33['score.raw'];
                    $table->add_data($row2);
                }
            }
        }else{
            // Technology Activity or Human Eye

            if(count($group)>0){
                // build table content - Interactions + Objectives
                for ($i=0; $i < count($group) ; $i++) {
                    $line.= "<tr><td>".$group[$i]['question_id']."</td><td>".$group[$i]['correct_responses_pattern']."</td><td>".$group[$i]['description']."</td><td>".$group[$i]['id']."</td><td>".$group[$i]['learner_response']."</td><td>".$group[$i]['objectives_id']."</td><td>".$group[$i]['result']."</td><td>".$group[$i]['timestamp']."</td><td>".$group[$i]['type']."</td><td>".$group[$i]['weighting']."</td></tr>";
                }

                // show data on the table
                echo "<table>
                        <thead>
                            <tr><th>Question Id</th><th>Correct Response</th><th>Description</th><th>Id</th><th>Learner Response</th><th>Objectives Id</th><th>Result</th><th>Timestamp</th><th>Type</th><th>Weighting</th></tr>
                        </thead>
                        <tbody>
                            $line
                        </tbody>
                      </table>";
            }else{
                echo "Nothing to show<br/><br/>";
            }
        }  

        $table->finish_output();

        return true;
    }

    /**
     * Returns The maximum numbers of Questions associated with a Sco object
     *
     * @param int $scoid Sco ID
     * @return int an integer representing the question count
     */

    protected static function get_sco_question_count($scoid) {
        global $DB;
        $count = 0;
        $params = array();
        $select = "scoid = ? AND ";
        $select .= $DB->sql_like("element", "?", false);
        $params[] = $scoid;
        $params[] = "cmi.interactions_%.id";
        $rs = $DB->get_recordset_select("scorm_scoes_track", $select, $params, 'element');
        $keywords = array("cmi.interactions_", ".id");
        if ($rs->valid()) {
            foreach ($rs as $record) {
                $num = trim(str_ireplace($keywords, '', $record->element));
                if (is_numeric($num) && $num > $count) {
                    $count = $num;
                }
            }
            // Done as interactions start at 0 (do only if we have something to report).
            $count++;
        }
        $rs->close(); // Closing recordset.
        return $count;
    }
}
