<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * This page displays the user data from a single attempt
 *
 * @package mod_scorm
 * @copyright 1999 onwards Martin Dougiamas {@link http://moodle.com}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->libdir.'/tablelib.php');

$id = required_param('id', PARAM_INT); // Course Module ID.
$userid = required_param('user', PARAM_INT); // User ID.
$scoid = required_param('scoid', PARAM_INT); // SCO ID.
$attempt = optional_param('attempt', 1, PARAM_INT); // attempt number.
//$download = optional_param('download', '', PARAM_ALPHA);

// This script works for records made by Puzzle and Tecnhology activities

    global $DB;

    $cm = get_coursemodule_from_id('scorm', $id, 0, false, MUST_EXIST);
    //echo "<br/>$id ";print_object($cm);echo "<br/>";
    $new = false;
    if($cm->course==2){
        $new = true;
    }
    //$DB->get_records($table, array $conditions=null, $sort='', $fields='*', $limitfrom=0, $limitnum=0) - we get array from this
    $tracks = $DB->get_records('scorm_scoes_track', array('userid' => $userid, 'scoid' => $scoid,
                                                          'attempt' => $attempt), '','*');
                                                          // order by: 'element ASC' / limit: 0,30
    //echo "<br/>";print_object($tracks);echo "<br/>";die();
    //show the total records for this attempt
    $totalelements = count($tracks);
    echo "total elements: ";print_r($totalelements);echo "<br/><br/>";

    //BEGIN TO PROCESS DATA RECEIVED
    $curr_id = 0;
    $group = array();
    $other = array();
    
    // Distinguish parameters from Puzzle or Technology Activity
    foreach ($tracks as $key => $obj) {
        $has_= false;
        if (stristr($obj->element, 'cmi.interactions_') == true) {
            $has_= true;break;
        }
    }

    // Here I'm going to group the records for each question
    if ($has_) {
        // Puzzle Activity - echo "Puzzle Activity";
        foreach ($tracks as $record => $obj) {
            //echo "<br/>";print_object($obj);echo "<br/>";
            //Swith between Interactions, Objectives or other recods
            $curElement = stristr(str_replace('cmi.', '', $obj->element), '_',true);
            //print_object($curElement);echo "<br/>";
            switch ($curElement) {
                
                case 'interactions':
                    
                    //get the ID and Name of the current element
                    $elemntIdName = str_replace(strstr($obj->element, '_',true).'_', '', $obj->element);
                    //get only the ID
                    $interaction_id = strstr($elemntIdName, '.',true);
                    //for each different question -> create a new group
                    $group[$interaction_id]['question_id'] = $interaction_id;
                    //get the name, to add to the group
                    $interaction_name = str_replace('.', '', strstr($elemntIdName, '.'));
                    //print_object($interaction_name);echo "<br/>";
                    //Compare to the current one to check if we can add to the same group
                    if ($curr_id==$interaction_id) {
                        //add to the current group
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }else{
                        //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                        if($interaction_name=='id'){
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }
                    }

                    //Update current id to be compared
                    $curr_id = $interaction_id;
                    
                    break;

                case 'objectives':

                    $objectives = str_replace('cmi.objectives_', '', $obj->element);
                    //get only the ID
                    $interaction_id = str_replace('cmi.objectives_', '', $obj->element);
                    $interaction_id = strstr(str_replace('cmi.objectives_', '', $obj->element), '.',true);

                    //get the name, to add to the group
                    $interaction_name = str_replace('cmi.objectives_'.$interaction_id.'.', '', $obj->element);

                    //Compare to the current one to check if we can add to the same group
                    if ($curr_id==$interaction_id) {
                        //add to the current group
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }else{
                        //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                        if($interaction_name=='score.raw'){
                            $group[$interaction_id][$interaction_name] = $obj->value;
                        }
                    }
                    //Update current id to be compared
                    $curr_id = $interaction_id;

                    break;
                
                default:
                    
                    if (stristr($obj->element, 'core') == true) {
                        $otherName = str_replace('cmi.core.', '', $obj->element);
                        $other[$otherName] = $obj->value;
                    }else{
                        $date = date('d/m/Y H:i:s', $obj->value);
                        $other[$obj->element] = $obj->value.' -> '.$date;
                    }
                    break;
            }
        }
    }
    else{
        //echo "Technology Activity";
        foreach ($tracks as $record => $obj) {

            //Swith between Interactions, Objectives or other recods
            $piece =str_replace('cmi.', '', $obj->element);
            $curElement = stristr($piece, '.',true);
            
            if (stristr($obj->element, 'cmi.interactions') !== false) {
                //get the ID and Name of the current element
                $elemntIdName = str_replace('cmi.interactions.', '',$obj->element);
                //get only the ID
                $interaction_id = strstr($elemntIdName, '.',true);
                //for each different question -> create a new group
                $group[$interaction_id]['question_id'] = $interaction_id;

                //Ignore the elements that don't match to what we are looking for
                if(stristr($obj->element, '.objectives.') !== false || stristr($obj->element, '.correct_responses.') !== false){

                    //get the first name
                    $interaction_1name = strstr(str_replace(strstr($elemntIdName, '.',true).'.', '',  $elemntIdName), '.',true);
                    //get the second name
                    $interaction_2name = str_replace('0.', '', strstr($obj->element, '0.'));
                    //its an exception for the case when the the id changes
                    if(stristr($interaction_2name, '.') !== false){
                        $interaction_2name = str_replace($interaction_1name.'.', '', $interaction_2name);
                    }
                    //add the two names, on a single string
                    $interaction_name = $interaction_1name.'_'.$interaction_2name;
                }
                else{
                    //get the name, to add to the group
                    $interaction_name = str_replace('.', '', strstr($elemntIdName, '.'));
                }
                
                //Compare to the current one to check if we can add to the same group
                if ($curr_id==$interaction_id) {
                    //add to the current group
                    $group[$interaction_id][$interaction_name] = $obj->value;
                }else{
                    //if it hasn't the same id value but it's the id element, then we add it because it's the start of a new group of elements
                    if($interaction_name=='objectives_id'){
                        $group[$interaction_id][$interaction_name] = $obj->value;
                    }
                }

                //Update current id to be compared
                $curr_id = $interaction_id;
            }
            else{
                if (stristr($obj->element, 'cmi.') == true) {
                    $field = str_replace('cmi.','', $obj->element);
                    $other[$field] = $obj->value;
                    if(stristr($obj->element, '_time') == true){
                        $other[$field] = scorm_format_duration($obj->value);
                    }
                }else{
                    $date = date('d/m/Y H:i:s', $obj->value);
                    $other[$obj->element] = $obj->value.' -> '.$date;
                }
            }
        }
    }
//die();
    //echo "<br/>";print_object($group);echo "<br/>";die();
    $line = '';$line2 = '';
    if($has_){
        
        // Maths Activity
        if($new){
            // build table content - Interactions + Objectives
            $css="style='text-align:center;vertical-align:middle;height:50px;width:100px;'";
            for ($i=0; $i < count($group); $i++) { 
                $line.= "<tr><td $css>".$group[$i]['question_id']."</td><td $css>".$group[$i]['id']."</td><td $css>".$group[$i]['correct_responses_0pattern']."</td><td $css>".$group[$i]['student_response']."</td><td $css>".$group[$i]['result']."</td><td $css>".$group[$i]['type']."</td></tr>";
            }

            // show data on the table
            echo "<table border='1'>
                    <thead>
                        <tr><th $css>Question Id</th><th $css>Id</th><th $css>Correct Response</th><th $css>Student Response</th><th $css>Result</th><th $css>Type</th></tr>
                    </thead>
                    <tbody>
                        $line
                    </tbody>
                  </table>";
        }else{
            // Puzzle Activity

            // build table content - Interactions + Objectives
            for ($i=0; $i < count($group); $i++) { 
                $line.= "<tr><td>".$group[$i]['question_id']."</td><td>".$group[$i]['id']."</td><td>".$group[$i]['result']."</td><td>".$group[$i]['type']."</td><td>".$group[$i]['time']."</td><td>".$group[$i]['score.raw']."</td></tr>";
            }

            // show data on the table
            echo "<table>
                    <thead>
                        <tr><th>Question Id</th><th>Id</th><th>Answer</th><th>Type</th><th>Time</th><th>Objectives (ScoreRaw)</th></tr>
                    </thead>
                    <tbody>
                        $line
                    </tbody>
                  </table>";
        }
        
    }else{
        // Technology Activity or Human Eye

        if(count($group)>0){
            // build table content - Interactions + Objectives
            for ($i=0; $i < count($group) ; $i++) {
                $line.= "<tr><td>".$group[$i]['question_id']."</td><td>".$group[$i]['correct_responses_pattern']."</td><td>".$group[$i]['description']."</td><td>".$group[$i]['id']."</td><td>".$group[$i]['learner_response']."</td><td>".$group[$i]['objectives_id']."</td><td>".$group[$i]['result']."</td><td>".$group[$i]['timestamp']."</td><td>".$group[$i]['type']."</td><td>".$group[$i]['weighting']."</td></tr>";
            }

            // show data on the table
            echo "<table>
                    <thead>
                        <tr><th>Question Id</th><th>Correct Response</th><th>Description</th><th>Id</th><th>Learner Response</th><th>Objectives Id</th><th>Result</th><th>Timestamp</th><th>Type</th><th>Weighting</th></tr>
                    </thead>
                    <tbody>
                        $line
                    </tbody>
                  </table>";
        }else{
            echo "Nothing to show<br/><br/>";
        }
    }    

    // Other parameters
    $css2="style='text-align:center;vertical-align:middle;height:50px;width:200px;'";
    foreach ($other as $key => $value) {
        $line2.="<tr><td $css2>".$key."</td><td $css2>".$value."</td></tr>";
    }

    // show data on the table
    echo "<br/>
        <table border=1>
            <thead>
                <tr><th $css2>Other Parameters</th><th $css2>Value</th></tr>
            </thead>
            <tbody>
                $line2
            </tbody>
        </table>";