<?php
/**
 * Created by PhpStorm.
 * User: ssmyth
 * Date: 17/05/2016
 * Time: 10:34
 */

class DB_Stuff {

    protected function retrieve_recordset($query) {

        global $DB;
        return $DB->get_recordset_sql( $query );

    }

    function get_recordset($query){
        return $this->retrieve_recordset($query);
    }

}