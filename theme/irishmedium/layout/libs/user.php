<?php
/**
 * Created by PhpStorm.
 * User: ssmyth
 * Date: 17/05/2016
 * Time: 10:12
 */



class SnasUser {

    public $hide_menu_from_teacher = false;
    public $hide_menu_from_student = false;
    public $can_edit = false;
    public $role = false;

    function __construct() {

        global $PAGE, $USER;
        //get context and get roles
        $context = context_course::instance($PAGE->course->id);
        $roles = get_user_roles($context, $USER->id, false);

        //convert for searching
        $role_array = objectToArray($roles);

        //decide the users role and set constant...

        //if teacher...
        if( search_array('editingteacher', $role_array) || search_array('teacher', $role_array) || search_array('coursecreator', $role_array) || search_array('manager', $role_array)){

            //set constant to teacher
            $this->role = 'teacher';

            //get id's of menus to hide for teachers
            $menu_ids = getInstancesOfMenu(getTableIDFromName('Daltai'), 'instanceid');
            //create classes to hide menus
            $hide_menu_from_teacher = createDivs($menu_ids);

            $this->hide_menu_from_teacher = $hide_menu_from_teacher;

        //if student...
        }else if(search_array('student', $role_array)){

            //set constant to student
            $this->role = 'student';

            //get id's of menus to hide for students
            $menu_ids = getInstancesOfMenu(getTableIDFromName('Muinteoiri amhain'), 'instanceid');
            //create classes to hide menus
            $this->hide_menu_from_student = createDivs($menu_ids);

        //otherwise must be admin
        }else{
            //set constant to admin
            if(is_admin()){
                $this->role = 'admin';
                $this->can_edit = true;
            }
        }

    }

    function get_user_role(){
        return $this->role;
    }

}

$snasUser = new SnasUser();