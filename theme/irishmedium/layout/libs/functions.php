<?php

require_once($CFG->dirroot.'/theme/irishmedium/layout/libs/db_stuff.php');


function create_array() {

    global $PAGE;
    $DB_Stuff = new DB_Stuff();

    //array of all the parent pages
    $parent_pages = make_parent_array( $DB_Stuff->get_recordset('SELECT id, name , parentid FROM mdl_format_flexpage_page WHERE courseid = '.$PAGE->course->id.';') );
    //print_object($parent_pages);die;


    //get array of children
    $child_page_array = create_child_array( $DB_Stuff->get_recordset('SELECT id, name , parentid FROM mdl_format_flexpage_page WHERE courseid = '.$PAGE->course->id.';'), $parent_pages );
    //print_object($child_page_array);die;


    $page_array = make_page_array( $DB_Stuff->get_recordset('SELECT id, name , parentid FROM mdl_format_flexpage_page WHERE courseid = '.$PAGE->course->id.';') );
    //print_object($page_array);


    //rearrange the array of children so video comes first, audio comes second... etc.
    $child_page_array = rearrange($child_page_array, $page_array);
    //print_object($child_page_array);die;


    //complete units array, children and reset keys
    $complete_unit_array = add_parents($child_page_array, $parent_pages);
    //print_object($complete_unit_array);die;


    //complete array with all pages
    return complete_array($page_array, $complete_unit_array);
    //print_object($complete_array);die;
}



function class_creator($array, $allPages){

    global $PAGE;
    $page_class = '';

    foreach ($array as $key => $value) {
        //if instance is array...
        if(is_array($value)){
            //and pageid is in this array...
            if(in_array($PAGE->subpage, $value)){
                //set key value as the page_id (used for css targeting)
                $page_class = 'pageClass-' . ($key + 1);
            }
        }else if($PAGE->subpage == $value){
            //if not, set a default to 1
            $page_class = 'pageClass-1';
        }
    }

    //convert value to string for searching
    $new_val = strval($PAGE->subpage);
    //search all pages array for value and get the key
    $make_key = get_key_from_array($allPages, 'id', $new_val);

    //add a unique class to the $page_class variable
    $page_class .= ' uniqueid-'.$make_key;
    return $page_class;

}



function snas_redirect($array){

    global $PAGE;

    //redirect to video page for ALL units.
    foreach ($array as $key => $value) {

        if(is_array($value)){
            if($value[0] == $PAGE->subpage){
                redirect('view.php?id='.$PAGE->course->id.'&pageid='.$value[1]);
            }
        }
    }
}


function complete_array($array1, $array2) {

    //put rest of pages into the new array
    foreach ($array1 as $key1 => $val){

        if(!search_array($val['id'], $array2)) {

            //if 'home' put at start of array
            if($val['name'] == 'Home' || $val['name'] == 'home'){
                array_unshift($array2, $val['id']);
            }else {
                $array2[] = $val['id'];
            }
        }
    }

    return $array2;
}


function add_parents($array, $pages_array) {
    //add parents id to beginning of array of children
    foreach ($array as $key => $val){
        array_unshift($array[$key],$pages_array[$key]['id']);
    }

    //reset keys of array and remove all empty arrays
    $array = array_values($array);
    $array = array_filter($array);

    return $array;
}


function rearrange($array, $allPAges) {


    //rearrange individual arrays so that 'Video' comes first
    foreach($array as $key => $val){
        $get_key = get_key_from_array($allPAges, 'id', $val[0]);
        $first_part = $allPAges[$get_key]['name'];
        $first_part = Unaccent($first_part);

        if($first_part != 'Fiseain'){
            $reversed = array_reverse($val);
            $array[$key] = $reversed;
        }
    }

    return $array;

}

function create_child_array( $array, $parents ){

    //loop through all the pages,
    foreach ($array as $key => $page){

        //if page is child,
        if($page->parentid != 0){
            //find its parents key
            $result = array_search($page->parentid, array_column($parents, 'id'));
            //and put the child page id into an array, based on the parent id
            $new_array[$result][] = $page->id;
        }
    }

    if(isset($new_array) && !empty($new_array)){
        return $new_array;
    }
    return false;
}


function hide_admin_menu($menu_ids){
    //if the query returns data...
    if(isset($menu_ids) && !empty($menu_ids)){

        //check to see if user is admin
        if(!is_admin()){

            //create an array with CSS div selectors
            foreach($menu_ids as $key => $val){
                $admin_menus[] = 'div[data-instanceid="'.$val->id.'"]';
            }
            //make string from array
            $divs = implode(', ', $admin_menus);
            //make complete class declaration for head section to hide menus
            return "<style>".$divs." {display: none;}.block .header .block_action img{display: none;}</style>";
        }

    }
    return false;
}


function make_parent_array($array){
    foreach ($array as $key => $page){

        if($page->parentid == 0){
            $pages_array[] = (array) $page;
        }
    }
    if(isset($pages_array) && !empty($pages_array)){
        return $pages_array;
    }
    return 'Could not make array of all parent pages';
}


function make_page_array($array){
    //make array of all pages
    foreach($array as $key => $val){
        $all_pages[] = $val;
    }
    //convert to an array
    if(isset($all_pages) && !empty($all_pages)){
        return objectToArray($all_pages);
    }
    return 'Could not create array of all pages';
}


//get menu id's based on table name
function getTableIDFromName($menu_name){

    global  $DB,$PAGE;

    //get record of menu item based on 'menu name'
    $cquery =   'SELECT `id` FROM `mdl_block_flexpagenav_menu` WHERE `name` = "'.$menu_name.'" and courseid = '.$PAGE->course->id.';';
    $menu_query = $DB->get_recordset_sql( $cquery );

    //get record id
    if($menu_query){
        foreach($menu_query as $key => $val){
            if($key = 'id') {
                return $val->id;
                break;
            }
        }
    }
    return false;
}

//get menu id's based on table name
function getInstancesOfMenu($menu_id, $table_column){

    global  $DB;

    //use id to find all menu id instances
    $cquery = 'SELECT `instanceid` FROM `mdl_block_flexpagenav` WHERE `menuid` = "'.$menu_id.'"';
    $menu_query = $DB->get_recordset_sql( $cquery );

    //get their id's
    if($menu_query){
        foreach($menu_query as $key => $val){
            if($key = $table_column)
                $teacher_menu_ids[] = ($val->$table_column);
        }
    }

    //if there are id's, return them
    if(isset($teacher_menu_ids) && !empty($teacher_menu_ids)){
        return $teacher_menu_ids;
    }else{
        return false;
    }
}

//create divs from array
function createDivs($array) {

    //create an array with CSS div selectors
    foreach($array as $key => $val){
        $menu_divs[] = 'div[data-instanceid="'.$val.'"]';
    }
    //make complete class declaration for head section to hide menus
    if(isset($menu_divs) && !empty($menu_divs)){
        return "<style>".implode(', ', $menu_divs)." {display: none;}</style>";
    }
    return false;

}

//replaces all accent characters for regular characters
function Unaccent($string){
    return preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
}


//change object into an array
function objectToArray ($object) {
    if(!is_object($object) && !is_array($object))
        return $object;

    return array_map('objectToArray', (array) $object);
}


//function for searching arrays
function search_array($needle, $haystack) {
    if(in_array($needle, $haystack)) {
        return true;
    }
    foreach($haystack as $element) {
        if(is_array($element) && search_array($needle, $element))
            return true;
    }
    return false;
}

//function for returning key from array
function get_key_from_array($products, $field, $value)
{
    foreach ($products as $key => $product) {
        if ($product[$field] === $value)
            return $key;
    }
    return false;
}


//see if user is an admin (for hiding admin/navigation menus
function is_admin(){

    global $USER;

    $admins = get_admins();
    foreach($admins as $admin) {

        if ($USER->id == $admin->id) {
            return true;
            break;
        }
    }
    return false;
}


$parent_id = getParentID($PAGE->subpage);
$child_page_ids = getChildIDs($parent_id);
$page_menu = buildPageMenu($child_page_ids);




function getParentID($menu_id){

    global  $DB;

    $cquery = 'SELECT `parentid` FROM `mdl_format_flexpage_page` WHERE `id` = "'.$menu_id.'"';
    $menu_query = $DB->get_recordset_sql( $cquery );

    foreach($menu_query as $key => $val) {
        if(isset($val->parentid) && !empty($val->parentid)){
            return $val->parentid;
        }
    }
    return false;
}


function getChildIDs($parent_id){

    global  $DB;

    $cquery = 'SELECT `id`,`name` FROM `mdl_format_flexpage_page` WHERE `name` = "Fiseain" AND `parentid` = "'.$parent_id.'" OR `name` = "Eisteacht" AND `parentid` = "'.$parent_id.'" OR `name` = "Gniomhaiochtai" AND `parentid` = "'.$parent_id.'" ORDER BY `weight` ASC';
    $child_id_query = $DB->get_recordset_sql( $cquery );

    foreach($child_id_query as $key => $val) {
        $child_ids[$key] = $val->id;
        $child_ids[$key] = $val->name;
    }
    if(isset($child_ids) && (!empty($child_ids))){
        return $child_ids;
    }
    return false;
}


function buildPageMenu($child_page_ids){

    global  $PAGE;

    if(isset($child_page_ids) && (!empty($child_page_ids))){
        $html = '<div class="custom_menu custom_menu-'.$PAGE->subpage.'"><ul>';
        foreach($child_page_ids as $key => $val){
            if($key == $PAGE->subpage){
                $html .= '<li class="current_page"><a href="view.php?id='.$PAGE->course->id.'&pageid='.$key.'">'.$val.'</a></li>';
            }else{
                $html .= '<li><a href="view.php?id='.$PAGE->course->id.'&pageid='.$key.'">'.$val.'</a></li>';
            }

        }
        $html .= '</ul></div>';
        return $html;
    }
    return false;
}




