<?php

global  $PAGE;

require_once($CFG->dirroot.'/theme/irishmedium/layout/libs/user.php');


$DB_Stuff = new DB_Stuff();


//Generate array of every page's id and put them in order
$complete_array = create_array();




//perform redirects if page is parent of unit
snas_redirect($complete_array);





//get id's of admin and settings menus
$get_admin_menu_ids = $DB_Stuff->get_recordset('SELECT id FROM mdl_block_instances WHERE blockname = "navigation" UNION SELECT id FROM mdl_block_instances WHERE blockname = "settings";');
//print_object($get_admin_menu_ids);die;


//hide admin menus
$hide_admin_menus = hide_admin_menu($get_admin_menu_ids);
//print_object($hide_admin_menus);die;





//create styling and unique classes to be put into body
$page_class = class_creator($complete_array, make_page_array( $DB_Stuff->get_recordset('SELECT id, name , parentid FROM mdl_format_flexpage_page WHERE courseid = '.$PAGE->course->id.';')) );
//print_object($page_class);



$sample_var = array( 'Some Text', $CFG->wwwroot );

//get jquery
$PAGE->requires->jquery();

//if current page is child...
if (search_array($PAGE->subpage, $complete_array)) {

    //run child specific scripts
    $PAGE->requires->js( new moodle_url($CFG->wwwroot . '/theme/irishmedium/js/snas_scripts.js'));
    $PAGE->requires->js( new moodle_url($CFG->wwwroot . '/theme/irishmedium/html5lightbox/html5lightbox.js'));
}

$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/theme/irishmedium/js/get_request.js'));

//call this JS function (true = on page ready)
//$PAGE->requires->js_init_call('sab_init', $sample_var);