<?php
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
    <title><?php echo $PAGE->title; ?></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800,600' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
</head>

<body id="<?php p($PAGE->bodyid); ?>" class="<?php p($PAGE->bodyclasses); ?>">
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="page">

    THIS IS THE COURSE FRONT PAGE!

    <?php if ($PAGE->heading || (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar())) { ?>
        <div id="page-header">
            <div class="container">
                <?php if ($PAGE->heading) { ?>
                    <div class="main-header">
                        <div class="logo">
                            <img src="<?php echo $OUTPUT->pix_url('snasAgusBlas-header', 'theme'); ?>" alt="" />
                        </div>
                    </div>
                    <div class="headermenu"><?php
                    echo $OUTPUT->login_info();
                    if (!empty($PAGE->layout_options['langmenu'])) {
                        echo $OUTPUT->lang_menu();
                    }
                    echo $PAGE->headingmenu
                    ?></div><?php } ?>


                <div class="navbutton"> <?php echo $PAGE->button; ?></div>

            </div>
        </div>
    <?php } ?>

    <div id="page-content">
        <div class="container">
            <div id="region-main-box">
                <div id="region-post-box" class="row">



                    <div class="col-md-3 sidebar">
                        <?php if ($hassidepre OR (right_to_left() AND $hassidepost)) { ?>
                            <div id="region-pre" class="block-region">
                                <div class="region-content">
                                    <?php
                                    if (!right_to_left()) {
                                        echo $OUTPUT->blocks('side-pre');
                                    } elseif ($hassidepost) {
                                        echo $OUTPUT->blocks('side-post');
                                    } ?>

                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($hassidepost OR (right_to_left() AND $hassidepre)) { ?>
                            <div id="region-post" class="block-region">
                                <div class="region-content">
                                    <?php
                                    if (!right_to_left()) {
                                        echo $OUTPUT->blocks('side-post');
                                    } elseif ($hassidepre) {
                                        echo $OUTPUT->blocks('side-pre');
                                    } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>



                    <div id="region-main-wrap" class="col-md-9">

                        <div id="region-main">
                            <div class="region-content">
                                <?php echo core_renderer::MAIN_CONTENT_TOKEN ?>
                            </div>
                        </div>

                        <?php if (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar()) { ?>
                            <div class="navbar">
                                <?php echo $OUTPUT->navbar(); ?>
                            </div>
                        <?php } ?>

                        <div id="region-main-wrap">
                            <div id="region-main" class="block-region">
                                <div class="region-content">
                                    <?php echo $OUTPUT->blocks('main') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (empty($PAGE->layout_options['nofooter'])) { ?>
        <div id="page-footer" class="clearfix">

            <div class="container">
                <div class="row footer_sections">
                    <div class="col-md-4 footer_section">
                        <h2 class="footer_header">Sample Text</h2>
                        <div class="footer_content">
                            <p>Is f&#233;idir nach mbeidh m&#243;r&#225;n ar fad le r&#225; ag na dalta&#237; faoi &#225;bhar chomh teib&#237; leis seo agus nach mairfidh an ghn&#237;omha&#237;ocht i bhfad. Mar sin f&#233;in, is deis at&#225; ann a insint do na dalta&#237; go.</p>
                        </div>
                    </div>
                    <div class="col-md-4 footer_section">
                        <h2 class="footer_header">Sample Text</h2>
                    </div>
                    <div class="col-md-4 footer_section">
                        <h2 class="footer_header">Sample Text</h2>
                    </div>
                </div>
            </div>

            <?php echo $OUTPUT->login_info(); ?>

            <!--<p class="helplink"><?php //echo page_doc_link(get_string('moodledocslink')) ?></p>-->

            <?php //echo $OUTPUT->home_link();
            echo $OUTPUT->standard_footer_html(); ?>
        </div>
    <?php } ?>

</div>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>