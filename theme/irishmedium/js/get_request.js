/**
 * Created by ssmyth on 16/05/2016.
 */

$( document ).ready(function() {

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

            var obj = xmlhttp.responseText;
            var parsed = JSON.parse(obj);
            var arr = [];

            for(var x in parsed){
                arr.push(parsed[x]);
            }

            setSelectOptions($('#name_selector'), arr, 'id', 'name');

        }
    };

    xmlhttp.open("GET","http://localhost/ks3_moodle/theme/irishmedium/js/get_data.php");
    xmlhttp.send();

});




function showUser(str) {

    document.getElementById("txtHint").innerHTML = "";

    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                console.log(xmlhttp.responseText);
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;

            }
        };

        //xmlhttp.open("GET","http://localhost/ks3_moodle/theme/irishmedium/js/get_data2.php");
        xmlhttp.open("GET","http://localhost/ks3_moodle/grade/report/scorm/index.php?user="+str+"&id=");
        xmlhttp.send();
    }
}




function setSelectOptions(selectElement, values, valueKey, textKey, defaultValue) {

    if (typeof (selectElement) == "string") {
        selectElement = $(selectElement);
    }

    selectElement.empty();

    if (typeof (values) == 'object') {

        if (values.length) {

            var type = typeof (values[0]);
            var html = "";

            if (type == 'object') {
                // values is array of hashes
                var optionElement = null;

                $.each(values, function () {
                    html += '<option value="' + this[valueKey] + '">' + this[textKey] + '</option>';
                });

            } else {
                // array of strings
                $.each(values, function () {
                    var value = this.toString();
                    html += '<option value="' + value + '">' + value + '</option>';
                });
            }

            selectElement.append(html);
        }

        // select the defaultValue is one was passed in
        if (typeof defaultValue != 'undefined') {
            selectElement.children('option[value="' + defaultValue + '"]').attr('selected', 'selected');
        }

    }

    return false;

}






//function called at the very bottom of code.php
function sab_init(Y,message,http) {

    //var action = question.one('#id_d_id');
    console.log(http);

    //console.log(question);
}