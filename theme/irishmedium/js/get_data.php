<?php
/**
 * Created by PhpStorm.
 * User: ssmyth
 * Date: 16/05/2016
 * Time: 11:46
 */


require_once '../../../config.php';//has CFG variable
global $DB;

$cquery =   'SELECT * FROM mdl_user';
$nav_id = $DB->get_recordset_sql( $cquery );

foreach ($nav_id as $key => $val){
    $name = $val->firstname . ' ' . $val->lastname;

    $my_Array = ['id' => $val->id, 'name' => $name];

    $export_array[] = $my_Array;
}

//echo json_encode($export_array);
echo json_encode($export_array);