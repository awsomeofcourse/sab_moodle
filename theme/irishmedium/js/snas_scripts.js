/**
 * Created by ssmyth on 13/05/2016.
 */

$( document ).ready(function() {

    var $menu = $("div.custom_menu");
    var $target = $("div.build_menu_here")

    if( $menu.length && $target.length ) {
        $("div.build_menu_here").replaceWith($("div.custom_menu"));
        $("div.custom_menu").css("display", "block");
    }else{
        $("div.build_menu_here").hide();
        $("div.custom_menu").hide();
    }
});